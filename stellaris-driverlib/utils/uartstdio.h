/*
 * uartstdio.h
 *
 *  Created on: Dec 26, 2019
 *      Author: Roger John <roger.john@rub.de>
 *
 *  The default utils/uartstdio in the driverlib is licensed under a closed
 *  license which does not permit public sharing. Therefore this file shall be
 *  an implementation under GPLv3.
 */


#ifndef UTILS_UARTSTDIO_H_
#define UTILS_UARTSTDIO_H_

#include <stddef.h>

int UARTStdioConfig(unsigned long portNum,
		unsigned long baudRate, unsigned long clockRate);
int UARTStdioInit(unsigned long portNum);
int UARTStdioInitExpClk(unsigned long portNum, unsigned long baudRate);
char UARTpeek(void);
char UARTgetc(void);
size_t UARTgets(char *str, size_t len);
void UARTputc(char x);
size_t UARTwrite(const char *str, size_t len);
size_t UARTwriteStr(const char *str);
size_t UARTprintf(const char *formatStr, ...);

#endif /* UTILS_UARTSTDIO_H_ */
