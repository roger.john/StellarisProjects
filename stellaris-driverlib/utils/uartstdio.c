/*
 * uartstdio.c
 *
 *  Created on: Dec 26, 2019
 *      Author: Roger John <roger.john@rub.de>
 *
 *  The default utils/uartstdio in the driverlib is licensed under a closed
 *  license which does not permit public sharing. Therefore this file shall be
 *  an implementation under GPLv3.
 */

#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_uart.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include <stdarg.h>
#include "uartstdio.h"
#include "fnprintf.h"

typedef struct {
	unsigned long base;
	unsigned long periph;
	unsigned long interrupt;
} UARTResourceStruct;

static const UARTResourceStruct const UARTResources[] = { { UART0_BASE,
SYSCTL_PERIPH_UART0, INT_UART0 }, { UART1_BASE, SYSCTL_PERIPH_UART1,
INT_UART1 }, { UART2_BASE, SYSCTL_PERIPH_UART2, INT_UART2 } };

static unsigned long UARTStdioPortNumber, UARTStdioPortBase;
static long UARTStdioPeekChar;

int UARTStdioConfig(unsigned long portNum, unsigned long baudRate,
		unsigned long clockRate) {
	if (portNum >= sizeof(UARTResources) / sizeof(UARTResourceStruct))
		return -1;
	if (!MAP_SysCtlPeripheralPresent(UARTResources[portNum].periph))
		return -1;
	UARTStdioPortNumber = portNum;
	UARTStdioPortBase = UARTResources[portNum].base;
	UARTStdioPeekChar = -1;
	MAP_SysCtlPeripheralEnable(UARTResources[portNum].periph);
	MAP_UARTDisable(UARTStdioPortBase);
	MAP_UARTConfigSetExpClk(UARTStdioPortBase, clockRate, baudRate,
	UART_CONFIG_PAR_NONE | UART_CONFIG_STOP_ONE | UART_CONFIG_WLEN_8);
	MAP_UARTFIFOEnable(UARTStdioPortBase);
	MAP_UARTEnable(UARTStdioPortBase);
	return 0;
}

int UARTStdioInit(unsigned long portNum) {
	return UARTStdioConfig(portNum, 115200, MAP_SysCtlClockGet());
}

int UARTStdioInitExpClk(unsigned long portNum, unsigned long baudRate) {
	return UARTStdioConfig(portNum, baudRate, MAP_SysCtlClockGet());
}

char UARTpeek(void) {
	if (UARTStdioPeekChar < 0)
		UARTStdioPeekChar = MAP_UARTCharGet(UARTStdioPortBase);
	return (unsigned char) UARTStdioPeekChar;
}

char UARTgetc(void) {
	if (UARTStdioPeekChar < 0)
		return (unsigned char) MAP_UARTCharGet(UARTStdioPortBase);
	long oldPeekChar = UARTStdioPeekChar;
	UARTStdioPeekChar = MAP_UARTCharGet(UARTStdioPortBase);
	return (unsigned char) oldPeekChar;
}

size_t UARTgets(char *str, size_t len) {
	if (str == 0 || len == 0)
		return 0;
	unsigned long i = 0;
	while (i < len) {
		unsigned char x = UARTgetc();
		if (x == '\r' || x == '\n' || x == '\x1b') {
			if (x == '\r' && UARTpeek() == '\n')
				UARTgetc();
			str[i] = 0;
			return i;
		}
		str[i++] = x;
	}
	return i;
}

void UARTputc(char x) {
	MAP_UARTCharPut(UARTStdioPortBase, x);
}

size_t UARTwrite(const char *str, size_t len) {
	if (str == 0)
		return 0;
	size_t i = 0;
	if (len > 0)
		while (i < len)
			MAP_UARTCharPut(UARTStdioPortBase, str[i++]);
	else
		while (*str)
			MAP_UARTCharPut(UARTStdioPortBase, *(str++));
	return i;
}

size_t UARTwriteStr(const char *str) {
	return UARTwrite(str, 0);
}

size_t UARTprintf(const char *formatStr, ...) {
	va_list ap;
	va_start(ap, formatStr);
	size_t r = vfnprintf(UARTwrite, formatStr, ap);
	va_end(ap);
	return r;
}
