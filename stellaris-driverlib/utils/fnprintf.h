/*
 * fnprintf.h
 *
 *  Created on: Dec 30, 2019
 *      Author: rj
 */

#ifndef UTILS_FNPRINTF_H_
#define UTILS_FNPRINTF_H_
#include <stddef.h>
#include <stdarg.h>

#ifndef tBoolean
typedef unsigned char tBoolean;
#endif
#ifndef true
#define true 1
#endif
#ifndef false
#define false 0
#endif

size_t fnprintf(size_t (*write)(const char*, size_t), const char *formatStr,
		...);
size_t vfnprintf(size_t (*write)(const char*, size_t), const char *formatStr,
		va_list ap);

#endif /* UTILS_FNPRINTF_H_ */
