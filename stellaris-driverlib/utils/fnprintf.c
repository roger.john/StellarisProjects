/*
 * fnprintf.c
 *
 *  Created on: Dec 30, 2019
 *      Author: rj
 */
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <wchar.h>
#include <stdint.h>
#include <float.h>
#include <math.h>
#include "fnprintf.h"

static int fnprintHexNibbles(size_t (*write)(const char*, size_t), uintmax_t x,
		int nibbles, tBoolean capital) {
	if (nibbles <= 0)
		return 0;
	if (nibbles > sizeof(uintmax_t) * 2)
		nibbles = sizeof(uintmax_t) * 2;
	int i = nibbles - 1, r = 0;
	while (i >= 0) {
		char y = ((x >> (4 * (i--))) & 0xF);
		if (x < 10)
			y += '0';
		else if (capital)
			y += 'A' - 10;
		else
			y += 'a' - 10;
		write(&y, 1);
		r++;
	}
	return r;
}

static int fnprintWChar(size_t (*write)(const char*, size_t), wint_t c) {
	int r = 0;
	if (c <= 0xFF) {
		write((char*) &c, 1);
		r++;
	} else if (c <= 0xFFFF) {
		r += write("\\u", 0);
		r += fnprintHexNibbles(write, c, 4, false);
	} else {
		r += write("\\U", 0);
		r += fnprintHexNibbles(write, c, 8, false);
	}
	return r;
}

static int sprintNumber(uintmax_t x, unsigned int base, char *s, int slen,
		tBoolean capital) {
	if (slen <= 0)
		return 0;
	int i = slen - 1;
	while (i >= 0) {
		unsigned int y = x % base;
		if (y < 10)
			s[i] = '0' + y;
		else if (capital)
			s[i] = 'A' + y - 10;
		else
			s[i] = 'a' + y - 10;
		x /= base;
		if (x == 0)
			break;
		else
			i--;
	}
	return slen - i;
}

long double frexp10l(long double x, int *exp) {
	if (!exp)
		return x;
	*exp = 0;
	if (x == 0. || isnanl(x) || isinfl(x))
		return x;
	int r = 0;
	tBoolean negative = x < 0;
	if (negative)
		x = -x;
	while (x < 1.L) {
		x *= 10.L;
		r--;
	}
	while (x >= 10.L) {
		x /= 10.L;
		r++;
	}
	*exp = r;
	return negative ? -x : x;
}

#define max(a,b) (a)<(b)?(b):(a)
#define min(a,b) (a)>(b)?(b):(a)

size_t fnprintf(size_t (*write)(const char*, size_t), const char *formatStr,
		...) {
	va_list ap;
	va_start(ap, formatStr);
	size_t r = vfnprintf(write, formatStr, ap);
	va_end(ap);
	return r;
}

size_t vfnprintf(size_t (*write)(const char*, size_t), const char *formatStr,
		va_list ap) {
	if (!write)
		return 0;
	const char *p = formatStr;
	const char *pSt;
	size_t cw = 0;
	char numBuf[max(3 * sizeof(long long), 3 * sizeof(long double) + 2)],
			expBuf[8];
	int numDigits, numExpDigits, numTrailingZeros = 0;
	size_t slen;
	long double xld, xn;
	int fexp;
	while (*p != 0) {
		tBoolean padWithZero = false, printPositiveSign = false, leftJustify =
				false, hashFlag = false, printSpaceForPlus = false, negative =
				false, expNegative = false, gIsExponential = false, roundUp =
				false;
		int width = -1, precision = -1, padding = 0;
		union {
			char c;
			unsigned char uc;
			wchar_t wc;
			wint_t wi;
			short int si;
			unsigned short int usi;
			int i;
			unsigned int ui;
			long l;
			unsigned long ul;
			long long ll;
			unsigned long long ull;
			intmax_t im;
			uintmax_t uim;
			size_t st;
			char *s;
			wchar_t *ws;
			void *v;
		} x;
		x.uim = 0ll;
		enum {
			CHAR, SHORT, INT, LONG, LONGLONG, INTMAX, SIZE, PTR, BIGLONG
		} length = INT;
		if (*p == '%') {
			pSt = p;
			p++;
			if (*p == '%') {
				cw += write("%", 1);
				p++;
				continue;
			}
			tBoolean flagsDone = false;
			while (!flagsDone) {
				switch (*p) {
				case '0':
					padWithZero = true;
					p++;
					break;
				case '+':
					printPositiveSign = true;
					p++;
					break;
				case '-':
					leftJustify = true;
					p++;
					break;
				case '#':
					hashFlag = true;
					p++;
					break;
				case ' ':
					printSpaceForPlus = true;
					p++;
					break;
				default:
					flagsDone = true;
					break;
				}
			}
			if (*p >= '1' && *p <= '9') {
				width = (int) (*p - '0');
				p++;
				while (*p >= '0' && *p <= '9') {
					width = width * 10 + (int) (*p - '0');
					p++;
				}
			} else if (*p == '*') {
				width = va_arg(ap, int);
				p++;
			}
			if (*p == '.') {
				p++;
				if (*p >= '0' && *p <= '9') {
					precision = (int) (*p - '0');
					p++;
					while (*p >= '0' && *p <= '9') {
						precision = precision * 10 + (int) (*p - '0');
						p++;
					}
				} else if (*p == '*') {
					precision = va_arg(ap, int);
					p++;
				} else
					precision = 0;
			}
			switch (*p) {
			case 'h':
				p++;
				if (*p == 'h') {
					length = CHAR;
					p++;
				} else
					length = SHORT;
				break;
			case 'l':
				p++;
				if (*p == 'l') {
					length = LONGLONG;
					p++;
				} else
					length = LONG;
				break;
			case 'j':
				p++;
				length = INTMAX;
				break;
			case 'z':
				p++;
				length = SIZE;
				break;
			case 't':
				p++;
				length = PTR;
				break;
			case 'L':
				p++;
				length = BIGLONG;
				break;
			}
			switch (*p) {
			case 'c':
				if (length == INT)
					x.i = va_arg(ap, int);
				else if (length == LONG)
					x.wi = va_arg(ap, wint_t);
				else {
					cw += write(pSt, p - pSt + 1);
					break;
				}
				padding = width - 1;
				if (!leftJustify)
					while ((padding--) > 0)
						cw += write(" ", 1);
				cw += fnprintWChar(write, x.wi);
				if (leftJustify)
					while ((padding--) > 0)
						cw += write(" ", 1);
				break;
			case 's':
				if (length == INT) {
					x.s = va_arg(ap, char*);
					slen = strlen(x.s);
				} else if (length == LONG) {
					x.ws = va_arg(ap, wchar_t*);
					slen = wcslen(x.ws);
				} else {
					cw += write(pSt, p - pSt + 1);
					break;
				}
				if (precision >= 0)
					slen = min(precision, slen);
				padding = width - slen;
				if (!leftJustify)
					while ((padding--) > 0)
						cw += write(" ", 1);
				if (length == INT)
					cw += write(x.s, slen);
				else
					while (*x.ws)
						cw += fnprintWChar(write, *(x.ws++));
				if (leftJustify)
					while ((padding--) > 0)
						cw += write(" ", 1);
				break;
			case 'd':
			case 'i':
			case 'u':
				if (length == BIGLONG) {
					cw += write(pSt, p - pSt + 1);
					break;
				}
				switch (length) {
				case CHAR:
				case SHORT:
				case INT:
					if (*p != 'u') {
						x.i = va_arg(ap, int);
						if (x.i < 0) {
							negative = true;
							x.ui = -x.i;
						}
					} else
						x.ui = va_arg(ap, unsigned int);
					break;
				case LONG:
					if (*p != 'u') {
						x.l = va_arg(ap, long);
						if (x.l < 0) {
							negative = true;
							x.ul = -x.l;
						}
					} else
						x.ul = va_arg(ap, unsigned long);
					break;
				case LONGLONG:
					if (*p != 'u') {
						x.ll = va_arg(ap, long long);
						if (x.ll < 0) {
							negative = true;
							x.ull = -x.ll;
						}
					} else
						x.ull = va_arg(ap, unsigned long long);
					break;
				case INTMAX:
					if (*p != 'u') {
						x.im = va_arg(ap, intmax_t);
						if (x.im < 0) {
							negative = true;
							x.uim = -x.im;
						}
					} else
						x.uim = va_arg(ap, uintmax_t);
					break;
				case SIZE:
					x.st = va_arg(ap, size_t);
					break;
				case PTR:
					x.v = va_arg(ap, void*);
					break;
				default:
					break;
				}
				numDigits = sprintNumber(x.uim, 10, numBuf, sizeof(numBuf),
						false);
				if (precision == 0 && x.uim == 0)
					numDigits = 0;
				padding =
						precision > numDigits ?
								width - precision : width - numDigits;
				if (negative || printPositiveSign || printSpaceForPlus)
					padding--;
				if (!leftJustify && !padWithZero)
					while ((padding--) > 0)
						cw += write(" ", 1);
				if (negative)
					cw += write("-", 1);
				else if (printPositiveSign)
					cw += write("+", 1);
				else if (printSpaceForPlus)
					cw += write(" ", 1);
				if (!leftJustify && padWithZero)
					while ((padding--) > 0)
						cw += write("0", 1);
				while (numDigits < precision--)
					cw += write("0", 1);
				cw += write(numBuf + (sizeof(numBuf) - numDigits), numDigits);
				if (leftJustify)
					while ((padding--) > 0)
						cw += write(" ", 1);
				break;
			case 'X':
			case 'x':
			case 'o':
				if (length == BIGLONG) {
					cw += write(pSt, p - pSt + 1);
					break;
				}
				switch (length) {
				case CHAR:
				case SHORT:
				case INT:
					x.ui = va_arg(ap, unsigned int);
					break;
				case LONG:
					x.ul = va_arg(ap, unsigned long);
					break;
				case LONGLONG:
					x.ull = va_arg(ap, unsigned long long);
					break;
				case INTMAX:
					x.uim = va_arg(ap, uintmax_t);
					break;
				case SIZE:
					x.st = va_arg(ap, size_t);
					break;
				case PTR:
					x.v = va_arg(ap, void*);
					break;
				default:
					break;
				}
				numDigits = sprintNumber(x.uim, *p == 'o' ? 8 : 16, numBuf,
						sizeof(numBuf), *p == 'X');
				if (precision == 0 && x.uim == 0)
					numDigits = 0;
				padding =
						precision > numDigits ?
								width - precision : width - numDigits;
				if (hashFlag)
					padding -= *p == 'o' ? 1 : 2;
				if (!leftJustify && !padWithZero)
					while ((padding--) > 0)
						cw += write(" ", 1);
				if (hashFlag)
					cw += write(*p == 'o' ? "0" : *p == 'X' ? "0X" : "0x", 0);
				if (!leftJustify && padWithZero)
					while ((padding--) > 0)
						cw += write("0", 1);
				while (numDigits < precision--)
					cw += write("0", 1);
				cw += write(numBuf + (sizeof(numBuf) - numDigits), numDigits);
				if (leftJustify)
					while ((padding--) > 0)
						cw += write(" ", 1);
				break;
			case 'P':
			case 'p':
				x.v = va_arg(ap, void*);
				numDigits = sprintNumber(x.uim, 16, numBuf, sizeof(numBuf),
						*p == 'P');
				precision = sizeof(void*) * 2;
				padding =
						precision > numDigits ?
								width - precision : width - numDigits;
				if (!leftJustify)
					while ((padding--) > 0)
						cw += write(" ", 1);
				if (hashFlag)
					cw += write(*p == 'P' ? "0X" : "0x", 2);
				while (numDigits < precision--)
					cw += write("0", 1);
				cw += write(numBuf + (sizeof(numBuf) - numDigits), numDigits);
				if (leftJustify)
					while ((padding--) > 0)
						cw += write(" ", 1);
				break;
			case 'A':
			case 'a':
			case 'E':
			case 'e':
			case 'F':
			case 'f':
			case 'G':
			case 'g':
				if (length == INT)
					xld = va_arg(ap, double);
				else if (length == BIGLONG)
					xld = va_arg(ap, long double);
				else {
					cw += write(pSt, p - pSt + 1);
					break;
				}
				negative = signbit(xld);
				if (isnanl(xld) || isinfl(xld)) {
					padding = width - 3;
					padWithZero = false;
					numExpDigits = 0;
					numTrailingZeros = 0;
					precision = 0;
				} else {
					if (negative)
						xld = -xld;
					if (tolower(*p) == 'a') {
						xn = frexpl(xld, &fexp);
						while (xn < (length == BIGLONG ? 8 : 1)) {
							xn *= 2;
							fexp--;
						}
					} else {
						xn = frexp10l(xld, &fexp);
						if (tolower(*p) == 'g') {
							if (precision < 0)
								precision = 6;
							else if (precision == 0)
								precision = 1;
							if (fexp < -3 || fexp > precision)
								gIsExponential = true;
						}
					}
					if (precision < 0 && tolower(*p) != 'a')
						precision = tolower(*p) == 'e' ? 6 : 0;
					if (tolower(*p) == 'g')
						precision = max(precision - 1, 0);
					else if (tolower(*p) == 'f')
						precision += fexp;
					if (xn == 0.L) {
						numDigits = 1;
						numTrailingZeros = 0;
					} else {
						if (tolower(*p) == 'a' && precision < 0) {
							uintmax_t iPart = xn;
							long double y = xn - iPart;
							precision = 0;
							while (y > 0) {
								y *= 16.L;
								iPart = y;
								y = y - iPart;
								precision++;
							}
							numDigits = precision + 1;
							numTrailingZeros = 0;
						} else {
							long double y = xn;
							uintmax_t iPart;
							int lastNonRoundUpDigit = -1, lastZeroBlockStart =
									-1;
							int base = tolower(*p) == 'a' ? 16 : 10;
							for (int i = 0; i <= precision; i++) {
								iPart = y;
								if (iPart < base - 1)
									lastNonRoundUpDigit = i;
								if (iPart == 0) {
									if (lastZeroBlockStart < 0)
										lastZeroBlockStart = i;
								} else
									lastZeroBlockStart = -1;
								y = (y - iPart) * base;
							}
							if (2 * y >= base) {
								roundUp = true;
								if (lastNonRoundUpDigit < 0) { // impossible for 'a'/'A'
									xn = 1.L;
									fexp += 1;
									numDigits = 1;
									if (tolower(*p) == 'f'
											|| (tolower(*p) == 'g'
													&& !gIsExponential))
										numDigits += fexp;
									numTrailingZeros = precision;
								} else if (lastNonRoundUpDigit == 0
										&& tolower(*p) == 'a') {
									xn = 1.L; // xn=0x1.FFFF...p(exp) rounds to 0x2p(exp)=0x1p(exp+1)
									fexp++;
									numDigits = 1;
									numTrailingZeros = precision;
								} else {
									numDigits = 1 + lastNonRoundUpDigit;
									if (tolower(*p) == 'g')
										numTrailingZeros = 0;
									else
										numTrailingZeros = max(0,
												1 + precision - numDigits);
								}
							} else {
								numTrailingZeros = 0;
								if (tolower(*p) == 'g'
										&& lastZeroBlockStart >= 0)
									numDigits =
											gIsExponential ?
													lastZeroBlockStart :
											max(fexp + 1, lastZeroBlockStart);
								else
									numDigits = precision + 1;
							}
						}
					}
					if (fexp < 0)
						expNegative = true;
					if (tolower(*p) == 'f'
							|| (tolower(*p) == 'g' && !gIsExponential))
						numExpDigits = 0;
					else
						numExpDigits = sprintNumber(abs(fexp), 10, expBuf,
								sizeof(expBuf), false);
					padding = width - numDigits - numTrailingZeros;
					if (numExpDigits > 0)
						padding -= 2 + numExpDigits;
					if (hashFlag || fexp + 1 < numDigits + numTrailingZeros) // period
						padding--;
					if (tolower(*p) == 'a') // hex prefix
						padding -= 2;
				}
				if (negative || printPositiveSign || printSpaceForPlus) // sign
					padding--;
				if (!leftJustify && !padWithZero)
					while ((padding--) > 0)
						cw += write(" ", 1);
				if (negative)
					cw += write("-", 1);
				else if (printPositiveSign)
					cw += write("+", 1);
				else if (printSpaceForPlus)
					cw += write(" ", 1);
				if (tolower(*p) == 'a')
					cw += write(*p == 'A' ? "0X" : "0x", 2);
				if (!leftJustify && padWithZero)
					while ((padding--) > 0)
						cw += write("0", 1);
				if (isnanl(xld))
					cw += write(isupper(*p) ? "NAN" : "nan", 3);
				else if (isinfl(xld))
					cw += write(isupper(*p) ? "INF" : "inf", 3);
				else {
					int digit = fexp;
					int sexp =
							tolower(*p) == 'f'
									|| (tolower(*p) == 'g' && !gIsExponential) ?
									0 : fexp;
					int base = tolower(*p) == 'a' ? 16 : 10;
					while (fexp - digit < numDigits) {
						uintmax_t iPart = xn;
						xn = (xn - iPart) * base;
						if (roundUp && fexp - digit == numDigits - 1)
							iPart++;
						int shouldBe1 = sprintNumber(iPart, base, numBuf,
								sizeof(numBuf), *p == 'A');
						cw += write(numBuf + (sizeof(numBuf) - shouldBe1),
								shouldBe1);
						if (digit == sexp
								&& (hashFlag || fexp - digit < numDigits - 1
										|| (precision > 0 && tolower(*p) != 'g')))
							cw += write(".", 1);
						digit--;
					}
				}
				if (numTrailingZeros > 0) {
					int digit = fexp - numDigits;
					while (numTrailingZeros > 0) {
						if (digit == -1)
							cw += write(".", 1);
						cw += write("0", 1);
						digit--;
						numTrailingZeros--;
					}
				}
				if (numExpDigits > 0) {
					cw += write(
							tolower(*p) == 'a' ? isupper(*p) ? "P" : "p"
:																																																																													isupper(*p) ? "E" : "e", 1);
					cw += write(expNegative ? "-" : "+", 1);
					cw += write(expBuf + (sizeof(expBuf) - numExpDigits),
							numExpDigits);
				}
				if (leftJustify)
					while ((padding--) > 0)
						cw += write(" ", 1);
				break;
			case 'n':
				x.v = va_arg(ap, void*);
				switch (length) {
				case CHAR:
					*(unsigned char*) x.v = cw;
					break;
				case SHORT:
					*(unsigned short int*) x.v = cw;
					break;
				case INT:
					*(unsigned int*) x.v = cw;
					break;
				case LONG:
					*(unsigned long*) x.v = cw;
					break;
				case LONGLONG:
					*(unsigned long long*) x.v = cw;
					break;
				case INTMAX:
					*(uintmax_t*) x.v = cw;
					break;
				case SIZE:
					*(size_t*) x.v = cw;
					break;
				case PTR:
					*(unsigned long*) x.v = cw;
					break;
				default:
					cw += write(pSt, p - pSt + 1);
				}
				break;
			default:
				cw += write(pSt, p - pSt + 1);
			}
			p++;
		} else
			cw += write(p++, 1);
	}
	return cw;
}
