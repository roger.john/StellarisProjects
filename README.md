Some projects for the Texas Instruments Stellaris Launchpad.

* stellaris-driverlib: contains the free driverlib from TI together with an own
 implementation of uartstdio and printf
* StellarisSPI: An UART-to-SPI converter, allows talking to SPI parts using the
 virtual comport of the launchpad
* StellarisFilter: A two-channel digital filter bank (using adc as input and pwm
 as output)
