################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../LM4F_startup.c \
../main.c 

OBJS += \
./LM4F_startup.o \
./main.o 

C_DEPS += \
./LM4F_startup.d \
./main.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Summon Linux GCC C Compiler'
	arm-none-eabi-gcc -DPART_LM4F120H5QR -DTARGET_IS_BLIZZARD_RA2 -DUART_BUFFERED -I"/home/rj/workspace/stellaris-driverlib" -I/home/rj/stellaris/StellarisWare -O0 -Wall -Wa,-adhlns="$@.lst" -c -fmessage-length=0 -ffunction-sections -fdata-sections -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -g3 -gdwarf-2 -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


