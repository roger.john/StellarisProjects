/*
 * Copyright (c) 2013, Roger John
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Roger John nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Roger John ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Roger John BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * File:			main.c.
 * Author:		Roger John.
 * Version:		1.0.0.
 * Description:	Main file for Stellaris Filter.
 */

#include "main.h"
#include <math.h>
#include "inc/hw_memmap.h"
#include "inc/hw_nvic.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_timer.h"
#include "driverlib/fpu.h"
#include "driverlib/interrupt.h"
#include "driverlib/sysctl.h"
#include "driverlib/systick.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "utils/uartstdio.h"
#include "filter.h"
#include "ctrl.h"
#include "io.h"

#define COUNT_TO_DELAY 100
#define STEP 1

unsigned long samplePeriod;
//long stepA = STEP;
//long stepB = -STEP;
#ifdef PROFILE
unsigned long mainStartClock = 0;
long mainClocks = 0;
#endif
//long countToDelay = COUNT_TO_DELAY;
long dutyA, dutyB;
long adcA = 0, adcB = 0;
float x1, x2;
volatile unsigned long tick = 0;
float frequency = 1.;
float halfSamplePeriod;
float pwmCenterDutyA, pwmCenterDutyB, adcCenterValA, adcCenterValB;
float auxFreqA = AUX_TIMER_DEFAULT_FREQ, auxDutyA = 0.5, auxFreqB =
		AUX_TIMER_DEFAULT_FREQ, auxDutyB = 0.;

enum Mode mode = MODE_THROUGH;
enum Error lastError = ERROR_NONE;

// main function.
int main(void) {
	MAP_FPUEnable ();
	MAP_FPUStackingEnable ();
	MAP_SysCtlClockSet (
			SYSCTL_SYSDIV_2_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN
					| SYSCTL_XTAL_16MHZ);
	samplePeriod = MAP_SysCtlClockGet () / SAMPLE_RATE;
	halfSamplePeriod = (float) samplePeriod / 2.;
#ifdef EXT_PROFILE
	initExternalProfiling();
#endif
	initUART();
	initPWM();
	initAUXTimer();
	initADC();
	initFiltering();
	MAP_SysTickPeriodSet (MAP_SysCtlClockGet () / SYSTICKS_PER_SEC);
	MAP_SysTickEnable ();
	MAP_SysTickIntEnable ();
	MAP_IntMasterEnable ();

	MAP_SysCtlDelay (10 * (MAP_SysCtlClockGet () / 1000));
	syncIO();
	MAP_SysCtlDelay (10 * (MAP_SysCtlClockGet () / 1000));
	syncSysTick();

	loopUART_cmd();
	return 0;
}

float fA(float t) {
	return sinf(M_TWOPI * frequency * t);
}
float fB(float t) {
	return cosf(M_TWOPI * frequency * t);
}

void SysTickIntHandler() {
//	IntMasterDisable();
	__asm("cpsid i");
#ifdef PROFILE
//	mainStartClock = SysTickValueGet();
	mainStartClock = HWREG(NVIC_ST_CURRENT);
#endif
#ifdef EXT_PROFILE
	GPIO_ENABLE_PIN(EXT_PROFILE_MAIN_GPIO_BASE, EXT_PROFILE_MAIN_GPIO_PIN);
#endif
	float t;
	switch (mode) {
	case MODE_FREEZE:
		break;
	case MODE_FUNC_GEN:
		t = ((float) tick) / SYSTICKS_PER_SEC;
		setPWMDuties(/*fA(t)*/-1.,/*fB(t)*/1.);
		break;
	case MODE_USER:
//		TimerMatchSet(PWM_TIMER_BASE, TIMER_A, dutyA);
//		TimerMatchSet(PWM_TIMER_BASE, TIMER_B, dutyB);
		HWREG(PWM_TIMER_BASE + TIMER_O_TAMATCHR) = dutyA;
		HWREG(PWM_TIMER_BASE + TIMER_O_TBMATCHR) = dutyB;
		break;
	case MODE_THROUGH:
//		adcAf = scalbnf(adcA - 2048, -11);
//		adcBf = scalbnf(adcB - 2048, -11);
		getAdcValues(&x1, &x2);
		setPWMDuties(x1, x2);
		break;
	case MODE_FILTER:
		getAdcValues(&x1, &x2);
		filter(&x1, &x2, tick);
		setPWMDuties(x1, x2);
		break;
	default:
		if (lastError == ERROR_NONE) {
			lastError = ERROR_MODE;
			UARTprintf("\nunknown mode: %d\n", mode);
		}
		break;
	}
	tick++;
#include "inc/hw_udma.h"
#include "driverlib/udma.h"
	HWREG(UDMA_ENASET) = 1 << UDMA_CHANNEL_ADC0;
	HWREG(UDMA_ENASET) = 1 << UDMA_SEC_CHANNEL_ADC10;

#ifdef EXT_PROFILE
	GPIO_DISABLE_PIN(EXT_PROFILE_MAIN_GPIO_BASE, EXT_PROFILE_MAIN_GPIO_PIN);
#endif
#ifdef PROFILE
//	mainClocks = mainStartClock - SysTickValueGet();
	mainClocks = mainStartClock - HWREG(NVIC_ST_CURRENT);
#endif
//	IntMasterEnable();
	__asm("cpsie i");
}

long clock() {
	return MAP_SysCtlClockGet ();
}
