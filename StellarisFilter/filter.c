/*
 * filter.c
 *
 *  Created on: 04.04.2013
 *      Author: rj
 */

#include "ctrl.h"
#include "filter.h"
#include "utils/uartstdio.h"

#define USE_MFB_FILTERING
#define USE_OPTIMIZED_MFB
#define USE_FAST_MFB_CHAINING

static iir_biquad filterChain[MAX_CHAIN_LENGTH][2];
static mixer mixerChain[MAX_CHAIN_LENGTH + 1];
static unsigned char filterMixerConfig[MAX_CHAIN_LENGTH + 1];
#ifndef USE_MFB_FILTERING
static int chainLength = 0;
//void initFiltering(void) {}
#else
struct mixer_filter_block_struct;
typedef struct mixer_filter_block_struct mixer_filter_block;
#if defined(USE_OPTIMIZED_MFB) && defined (USE_FAST_MFB_CHAINING)
typedef void (*mfb_proc)(mixer_filter_block**, float*, float*);
#else
typedef void (*mfb_proc)(mixer_filter_block*, float*, float*);
#endif
struct mixer_filter_block_struct {
#ifdef USE_OPTIMIZED_MFB
	mfb_proc mixerAndFilterApplicator;
#else
	void (*mixerAndFilterApplicator)(mixer_filter_block*, float *, float *);
	float (*filter1Proc)(iir_biquad*, float);
	float (*filter2Proc)(iir_biquad*, float);
#endif
	mixer *m;
	iir_biquad *filter1, *filter2;
	mixer_filter_block *next;
};
static mixer_filter_block mfbChain[2][MAX_CHAIN_LENGTH];
static mixer_filter_block *mfbChainStart[2];
void initFiltering(void) {
	int i;
	for(i=0;i<MAX_CHAIN_LENGTH;i++) {
		mfbChain[0][i].m = mfbChain[1][i].m = &mixerChain[i];
		mfbChain[0][i].filter1 = mfbChain[1][i].filter1 = &filterChain[i][0];
		mfbChain[0][i].filter2 = mfbChain[1][i].filter2 = &filterChain[i][1];
	}
}

#ifdef USE_OPTIMIZED_MFB
#define filterApplicatorProcDefs \
		filterApplicatorProcDefs1(0)\
		filterApplicatorProcDefs1(1)
#define filterApplicatorProcDefs1(m) \
		filterApplicatorProcDefs2(m,0)\
		filterApplicatorProcDefs2(m,1)\
		filterApplicatorProcDefs2(m,2)\
		filterApplicatorProcDefs2(m,3)\
		filterApplicatorProcDefs2(m,4)
#define filterApplicatorProcDefs2(m,f1) \
		filterApplicatorProcDefs3(m,f1,0)\
		filterApplicatorProcDefs3(m,f1,1)\
		filterApplicatorProcDefs3(m,f1,2)\
		filterApplicatorProcDefs3(m,f1,3)\
		filterApplicatorProcDefs3(m,f1,4)
#define filterApplicatorProcDefs3(m,f1,f2) \
		filterApplicatorProcDef4(m,f1,f2,0)\
		filterApplicatorProcDef4(m,f1,f2,1)

#ifdef USE_FAST_MFB_CHAINING
#define filterApplicatorProcDef4(me,f1,f2,o) \
void filterApplicatorProcImpl##me##f1##f2##o(mixer_filter_block **mfb, float *x1, float *x2)\
	__attribute__((naked));\
void filterApplicatorProcImpl##me##f1##f2##o(mixer_filter_block **mfb, float *x1, float *x2) {\
		if(me) mix((*mfb)->m, x1, x2);\
		if(f1!=0) *x1 = filterProc##f1##o((*mfb)->filter1, *x1);\
		if(f2!=0) *x2 = filterProc##f2##o((*mfb)->filter2, *x2);\
		*mfb = (*mfb)->next;\
		goto *((*mfb)->mixerAndFilterApplicator);\
	}
#else
#define filterApplicatorProcDef4(me,f1,f2,o) \
	void filterApplicatorProcImpl##me##f1##f2##o(mixer_filter_block *mfb, float *x1, float *x2) {\
		if(me) mix(mfb->m, x1, x2);\
		if(f1!=0) *x1 = filterProc##f1##o(mfb->filter1, *x1);\
		if(f2!=0) *x2 = filterProc##f2##o(mfb->filter2, *x2);\
	}
#endif
#define filterApplicatorProc(m,f1,f2,o) \
	filterApplicatorProcImpl##m##f1##f2##o
#define filterProc10 filterSFIR_evenClock
#define filterProc11 filterSFIR_oddClock
#define filterProc20 filterSIIR_evenClock
#define filterProc21 filterSIIR_oddClock
#define filterProc30 filterBFIR_evenClock
#define filterProc31 filterBFIR_oddClock
#define filterProc40 filterBIIR_evenClock
#define filterProc41 filterBIIR_oddClock
#define filterApplicatorProcsArrayDef static const mfb_proc filterApplicatorProcsArray[] = {\
				filterApplicatorProcsArrayDef1(0),\
				filterApplicatorProcsArrayDef1(1)\
		};
#define filterApplicatorProcsArrayDef1(m) filterApplicatorProcsArrayDef2(m,0),\
				filterApplicatorProcsArrayDef2(m,1),\
				filterApplicatorProcsArrayDef2(m,2),\
				filterApplicatorProcsArrayDef2(m,3),\
				filterApplicatorProcsArrayDef2(m,4)
#define filterApplicatorProcsArrayDef2(m,f1) filterApplicatorProcsArrayDef3(m,f1,0),\
				filterApplicatorProcsArrayDef3(m,f1,1),\
				filterApplicatorProcsArrayDef3(m,f1,2),\
				filterApplicatorProcsArrayDef3(m,f1,3),\
				filterApplicatorProcsArrayDef3(m,f1,4)
#define filterApplicatorProcsArrayDef3(m,f1,f2) filterApplicatorProcsArrayDef4(m,f1,f2,0),\
				filterApplicatorProcsArrayDef4(m,f1,f2,1)
#define filterApplicatorProcsArrayDef4(m,f1,f2,o) filterApplicatorProc(m,f1,f2,o)

#endif

#ifdef USE_OPTIMIZED_MFB

filterApplicatorProcDefs
filterApplicatorProcsArrayDef

inline mfb_proc getFilterApplicator(tBoolean me, tBoolean e1, tBoolean b1,
				tBoolean i1, tBoolean e2, tBoolean b2, tBoolean i2, tBoolean o) {
	return filterApplicatorProcsArray[((me*5+(e1?b1?i1?4:3:i1?2:1:0))*5+(e2?b2?i2?4:3:i2?2:1:0))<<1| o];
}

#ifdef USE_FAST_MFB_CHAINING
void filterTerminatorProc(mixer_filter_block *mfb, float *x1, float *x2) {
	return;
}
const mixer_filter_block terminating_mfb = { filterTerminatorProc, NULL, NULL, NULL };
#endif

#else
void nopApplicator(mixer_filter_block *mfb, float *x1, float *x2) {
}
void filter1Applicator(mixer_filter_block *mfb, float *x1, float *x2) {
	*x1 = mfb->filter1Proc(mfb->filter1, *x1);
}
void filter2Applicator(mixer_filter_block *mfb, float *x1, float *x2) {
	*x2 = mfb->filter2Proc(mfb->filter2, *x2);
}
void filterBothApplicator(mixer_filter_block *mfb, float *x1, float *x2) {
	*x1 = mfb->filter1Proc(mfb->filter1, *x1);
	*x2 = mfb->filter2Proc(mfb->filter2, *x2);
}
void mixOnlyApplicator(mixer_filter_block *mfb, float *x1, float *x2) {
	mix(mfb->m, x1, x2);
}
void mixAndFilter1Applicator(mixer_filter_block *mfb, float *x1, float *x2) {
	mix(mfb->m, x1, x2);
	*x1 = mfb->filter1Proc(mfb->filter1, *x1);
}
void mixAndFilter2Applicator(mixer_filter_block *mfb, float *x1, float *x2) {
	mix(mfb->m, x1, x2);
	*x2 = mfb->filter2Proc(mfb->filter2, *x2);
}
void mixAndFilterBothApplicator(mixer_filter_block *mfb, float *x1, float *x2) {
	mix(mfb->m, x1, x2);
	*x1 = mfb->filter1Proc(mfb->filter1, *x1);
	*x2 = mfb->filter2Proc(mfb->filter2, *x2);
}
#endif

#endif

inline float filterBIIR(iir_biquad *f, float x0, tBoolean clockParity) {
	if (clockParity)
		return filterBIIR_oddClock(f, x0);
	else
		return filterBIIR_evenClock(f, x0);
}
inline float filterBIIR_oddClock(iir_biquad *f, float x0) {
	float y0 = x0 * f->a0 + f->x2 * f->a1 + f->x1 * f->a2 - f->y2 * f->b1
			- f->y1 * f->b2;
	f->x1 = x0;
	f->y1 = y0;
	return y0;
}
inline float filterBIIR_evenClock(iir_biquad *f, float x0) {
	float y0 = x0 * f->a0 + f->x1 * f->a1 + f->x2 * f->a2 - f->y1 * f->b1
			- f->y2 * f->b2;
	f->x2 = x0;
	f->y2 = y0;
	return y0;
}
inline float filterSIIR(iir_biquad *f, float x0, tBoolean clockParity) {
	if (clockParity)
		return filterSIIR_oddClock(f, x0);
	else
		return filterSIIR_evenClock(f, x0);
}
inline float filterSIIR_oddClock(iir_biquad *f, float x0) {
	float y0 = x0 * f->a0 + f->x2 * f->a1 - f->y2 * f->b1;
	f->x1 = x0;
	f->y1 = y0;
	return y0;
}
inline float filterSIIR_evenClock(iir_biquad *f, float x0) {
	float y0 = x0 * f->a0 + f->x1 * f->a1 - f->y1 * f->b1;
	f->x2 = x0;
	f->y2 = y0;
	return y0;
}

inline float filterBFIR(iir_biquad *f, float x0, tBoolean clockParity) {
	if (clockParity)
		return filterBFIR_oddClock(f, x0);
	else
		return filterBFIR_evenClock(f, x0);
}
inline float filterBFIR_oddClock(iir_biquad *f, float x0) {
	float y0 = x0 * f->a0 + f->x2 * f->a1 + f->x1 * f->a2;
	f->x1 = x0;
	f->y1 = y0;
	return y0;
}
inline float filterBFIR_evenClock(iir_biquad *f, float x0) {
	float y0 = x0 * f->a0 + f->x1 * f->a1 + f->x2 * f->a2;
	f->x2 = x0;
	f->y2 = y0;
	return y0;
}
inline inline float filterSFIR(iir_biquad *f, float x0, tBoolean clockParity) {
	if (clockParity)
		return filterSFIR_oddClock(f, x0);
	else
		return filterSFIR_evenClock(f, x0);
}
inline float filterSFIR_oddClock(iir_biquad *f, float x0) {
	float y0 = x0 * f->a0 + f->x2 * f->a1;
	f->x1 = x0;
	f->y1 = y0;
	return y0;
}
inline float filterSFIR_evenClock(iir_biquad *f, float x0) {
	float y0 = x0 * f->a0 + f->x1 * f->a1;
	f->x2 = x0;
	f->y2 = y0;
	return y0;
}

inline void mix(mixer *m, float *x1, float *x2) {
	float y1 = m->m[0][0] * *x1 + m->m[0][1] * *x2;
	*x2 = m->m[1][0] * *x1 + m->m[1][1] * *x2;
	*x1 = y1;
}

#ifdef USE_MFB_FILTERING
void reconfigureMfbChain() {
	int i;
	mfbChainStart[0] = mfbChainStart[1] = NULL;
	mixer_filter_block **lastBlockPtr1 = &mfbChainStart[0];
	mixer_filter_block **lastBlockPtr2 = &mfbChainStart[1];
	for (i = 0; i < MAX_CHAIN_LENGTH; i++) {
#ifdef USE_OPTIMIZED_MFB
		mfbChain[0][i].mixerAndFilterApplicator = getFilterApplicator(
				filterMixerConfig[i] & CONFIG_MIX,
				filterMixerConfig[i] & CONFIG_FILTER_1_ENABLE,
				!(filterMixerConfig[i] & CONFIG_FILTER_1_SINGLE),
				!(filterMixerConfig[i] & CONFIG_FILTER_1_FIR),
				filterMixerConfig[i] & CONFIG_FILTER_2_ENABLE,
				!(filterMixerConfig[i] & CONFIG_FILTER_2_SINGLE),
				!(filterMixerConfig[i] & CONFIG_FILTER_2_FIR), false);
		mfbChain[1][i].mixerAndFilterApplicator = getFilterApplicator(
				filterMixerConfig[i] & CONFIG_MIX,
				filterMixerConfig[i] & CONFIG_FILTER_1_ENABLE,
				!(filterMixerConfig[i] & CONFIG_FILTER_1_SINGLE),
				!(filterMixerConfig[i] & CONFIG_FILTER_1_FIR),
				filterMixerConfig[i] & CONFIG_FILTER_2_ENABLE,
				!(filterMixerConfig[i] & CONFIG_FILTER_2_SINGLE),
				!(filterMixerConfig[i] & CONFIG_FILTER_2_FIR), true);
		mfbChain[0][i].next = mfbChain[1][i].next = NULL;
		if (filterMixerConfig[i]
				& (CONFIG_MIX | CONFIG_FILTER_1_ENABLE | CONFIG_FILTER_2_ENABLE)) {
			*lastBlockPtr1 = &mfbChain[0][i];
			*lastBlockPtr2 = &mfbChain[1][i];
			lastBlockPtr1 = &mfbChain[0][i].next;
			lastBlockPtr2 = &mfbChain[1][i].next;
		}
#else
		if (filterMixerConfig[i] & CONFIG_MIX) {
			if (filterMixerConfig[i] & CONFIG_FILTER_1_ENABLE) {
				if (filterMixerConfig[i] & CONFIG_FILTER_2_ENABLE)
					mfbChain[0][i].mixerAndFilterApplicator =
							mixAndFilterBothApplicator;
				else
					mfbChain[0][i].mixerAndFilterApplicator =
							mixAndFilter1Applicator;
			} else {
				if (filterMixerConfig[i] & CONFIG_FILTER_2_ENABLE)
					mfbChain[0][i].mixerAndFilterApplicator =
							mixAndFilter2Applicator;
				else
					mfbChain[0][i].mixerAndFilterApplicator = mixOnlyApplicator;
			}
		} else {
			if (filterMixerConfig[i] & CONFIG_FILTER_1_ENABLE) {
				if (filterMixerConfig[i] & CONFIG_FILTER_2_ENABLE)
					mfbChain[0][i].mixerAndFilterApplicator =
							filterBothApplicator;
				else
					mfbChain[0][i].mixerAndFilterApplicator = filter1Applicator;
			} else {
				if (filterMixerConfig[i] & CONFIG_FILTER_2_ENABLE)
					mfbChain[0][i].mixerAndFilterApplicator = filter2Applicator;
				else
					mfbChain[0][i].mixerAndFilterApplicator = nopApplicator;
			}
		}
		mfbChain[1][i].mixerAndFilterApplicator =
				mfbChain[0][i].mixerAndFilterApplicator;
		if (filterMixerConfig[i]
				& (CONFIG_MIX | CONFIG_FILTER_1_ENABLE | CONFIG_FILTER_2_ENABLE)) {
			*lastBlockPtr1 = &mfbChain[0][i];
			*lastBlockPtr2 = &mfbChain[1][i];
			lastBlockPtr1 = &mfbChain[0][i].next;
			lastBlockPtr2 = &mfbChain[1][i].next;
			if (filterMixerConfig[i] & CONFIG_FILTER_1_FIR) {
				if (filterMixerConfig[i] & CONFIG_FILTER_1_SINGLE) {
					mfbChain[0][i].filter1Proc = filterSFIR_evenClock;
					mfbChain[1][i].filter1Proc = filterSFIR_oddClock;
				} else {
					mfbChain[0][i].filter1Proc = filterBFIR_evenClock;
					mfbChain[1][i].filter1Proc = filterBFIR_oddClock;
				}
			} else {
				if (filterMixerConfig[i] & CONFIG_FILTER_1_SINGLE) {
					mfbChain[0][i].filter1Proc = filterSIIR_evenClock;
					mfbChain[1][i].filter1Proc = filterSIIR_oddClock;
				} else {
					mfbChain[0][i].filter1Proc = filterBIIR_evenClock;
					mfbChain[1][i].filter1Proc = filterBIIR_oddClock;
				}
			}
			if (filterMixerConfig[i] & CONFIG_FILTER_2_FIR) {
				if (filterMixerConfig[i] & CONFIG_FILTER_2_SINGLE) {
					mfbChain[0][i].filter2Proc = filterSFIR_evenClock;
					mfbChain[1][i].filter2Proc = filterSFIR_oddClock;
				} else {
					mfbChain[0][i].filter2Proc = filterBFIR_evenClock;
					mfbChain[1][i].filter2Proc = filterBFIR_oddClock;
				}
			} else {
				if (filterMixerConfig[i] & CONFIG_FILTER_2_SINGLE) {
					mfbChain[0][i].filter2Proc = filterSIIR_evenClock;
					mfbChain[1][i].filter2Proc = filterSIIR_oddClock;
				} else {
					mfbChain[0][i].filter2Proc = filterBIIR_evenClock;
					mfbChain[1][i].filter2Proc = filterBIIR_oddClock;
				}
			}
		}
#endif
	}
#ifdef USE_FAST_MFB_CHAINING
	*lastBlockPtr1 = &terminating_mfb;
	*lastBlockPtr2 = &terminating_mfb;
#endif
}
#endif

void filter(float *f1, float *f2, unsigned long clockTick) {
#ifdef USE_MFB_FILTERING
	mixer_filter_block *mfb = mfbChainStart[clockTick & 1];
#ifdef USE_FAST_MFB_CHAINING
	__asm volatile (" push {r4,r5,r6} \n");
	mfb->mixerAndFilterApplicator(&mfb, f1, f2);
	__asm volatile (" pop {r4,r5,r6} \n");
#else
	while (mfb) {
		mfb->mixerAndFilterApplicator(mfb, f1, f2);
		mfb = mfb->next;
	}
#endif
#else
	int i;
	tBoolean clockParity = clockTick & 1;
	for (i = 0; i < chainLength; i++) {
		if (filterMixerConfig[i] & CONFIG_MIX)
		mix(&mixerChain[i], f1, f2);
		if (filterMixerConfig[i] & CONFIG_FILTER_1_ENABLE) {
			if (filterMixerConfig[i] & CONFIG_FILTER_1_FIR) {
				if (filterMixerConfig[i] & CONFIG_FILTER_1_SINGLE)
				*f1 = filterSFIR(&filterChain[i][0], *f1, clockParity);
				else
				*f1 = filterBFIR(&filterChain[i][0], *f1, clockParity);
			} else {
				if (filterMixerConfig[i] & CONFIG_FILTER_1_SINGLE)
				*f1 = filterSIIR(&filterChain[i][0], *f1, clockParity);
				else
				*f1 = filterBIIR(&filterChain[i][0], *f1, clockParity);
			}
		}
		if (filterMixerConfig[i] & CONFIG_FILTER_2_ENABLE) {
			if (filterMixerConfig[i] & CONFIG_FILTER_2_FIR) {
				if (filterMixerConfig[i] & CONFIG_FILTER_2_SINGLE)
				*f2 = filterSFIR(&filterChain[i][1], *f2, clockParity);
				else
				*f2 = filterBFIR(&filterChain[i][1], *f2, clockParity);
			} else {
				if (filterMixerConfig[i] & CONFIG_FILTER_2_SINGLE)
				*f2 = filterSIIR(&filterChain[i][1], *f2, clockParity);
				else
				*f2 = filterBIIR(&filterChain[i][1], *f2, clockParity);
			}
		}
	}
#endif
	if (filterMixerConfig[MAX_CHAIN_LENGTH] & CONFIG_MIX)
		mix(&mixerChain[MAX_CHAIN_LENGTH], f1, f2);
}

long getMixerStatusAt() {
	long i = parseInteger();
	if (i < 0 || i > MAX_CHAIN_LENGTH) {
		UARTprintf("getMixerStatusAt: mixer-index out of bounds: '%d'", i);
		return -1;
	}
	return filterMixerConfig[i] & CONFIG_MIX ? 1 : 0;
}

long setMixerStatusAt() {
	long i = parseInteger();
	if (i < 0 || i > MAX_CHAIN_LENGTH) {
		UARTprintf("setMixerStatusAt: mixer-index out of bounds: '%d'", i);
		return -1;
	}
	skipWS();
	if (!UARTexpect("setMixerStatusAt", ','))
		return -1;
	long s = parseInteger();
	if (s)
		filterMixerConfig[i] |= CONFIG_MIX;
	else
		filterMixerConfig[i] &= ~CONFIG_MIX;
#ifdef USE_MFB_FILTERING
	reconfigureMfbChain();
#else
	adjustChainLength();
#endif
	return 0;
}

long getMixerAt() {
	long i = parseInteger();
	if (i < 0 || i > MAX_CHAIN_LENGTH) {
		UARTprintf("getMixerAt: mixer-index out of bounds: '%d'", i);
		return -1;
	}
	char buf[16];
	UARTprintf("{");
	if (i >= 0 && i <= MAX_CHAIN_LENGTH) {
		floatToString(mixerChain[i].m[0][0], buf, sizeof(buf));
		UARTprintf("{%s,", buf);
		floatToString(mixerChain[i].m[0][1], buf, sizeof(buf));
		UARTprintf("%s},{", buf);
		floatToString(mixerChain[i].m[1][0], buf, sizeof(buf));
		UARTprintf("%s,", buf);
		floatToString(mixerChain[i].m[1][1], buf, sizeof(buf));
		UARTprintf("%s}", buf);
	}
	UARTprintf("}");
	return 0;
}

long setMixerAt() {
	long i = parseInteger();
	if (i < 0 || i > MAX_CHAIN_LENGTH) {
		UARTprintf("setMixerAt: mixer-index out of bounds: '%d'", i);
		return -1;
	}
	skipWS();
	if (!UARTexpect("setMixerAt", ','))
		return -1;
	skipWS();
	if (!UARTexpect("setMixerAt", '{'))
		return -1;
	if (parseFloatList(mixerChain[i].m[0], 2, true) < 2) {
		UARTprintf("setMixerAt: expected 2 floats for mixer %d row 1", i);
		return -1;
	}
	if (!UARTexpect("setMixerAt", ','))
		return -1;
	if (parseFloatList(mixerChain[i].m[1], 2, true) < 2) {
		UARTprintf("setMixerAt: expected 2 floats for mixer %d row 2", i);
		return -1;
	}
	if (!UARTexpect("setMixerAt", '}'))
		return -1;
	return 0;
}
long getFilterStatusAt() {
	long i = parseInteger(), j = 0;
	if (i < 1 || i > MAX_CHAIN_LENGTH) {
		UARTprintf("getFilterStatusAt: filter-index out of bounds: '%d'", i);
		return -1;
	}
	i--;
	skipWS();
	if (UARTpeek() == ',') {
		UARTgetc();
		j = parseInteger();
		if (j < 0 || j > 2) {
			UARTprintf(
					"getFilterStatusAt: sub-filter-index out of bounds: '%d'",
					j);
			return -1;
		}
	}
	long r = 0;
	switch (j) {
	case 0:
		if (filterMixerConfig[i] & CONFIG_FILTER_1_ENABLE)
			r |= 1;
		if (filterMixerConfig[i] & CONFIG_FILTER_2_ENABLE)
			r |= 2;
		break;
	case 1:
		if (filterMixerConfig[i] & CONFIG_FILTER_1_ENABLE)
			return 1;
		break;
	case 2:
		if (filterMixerConfig[i] & CONFIG_FILTER_2_ENABLE)
			return 1;
		break;
	}
	return r;
}
#ifndef USE_MFB_FILTERING

int adjustChainLength() {
	int i;
	for (i = MAX_CHAIN_LENGTH - 1; i >= 0; i--) {
		if (filterMixerConfig[i]
				& (CONFIG_MIX | CONFIG_FILTER_1_ENABLE | CONFIG_FILTER_2_ENABLE)) {
			chainLength = i + 1;
			return chainLength;
		}
	}
	chainLength = 0;
	return chainLength;
}
#endif

long setFilterStatusAt() {
	long s = parseInteger(), e, i, j = 0;
	if (s < 1 || s > MAX_CHAIN_LENGTH) {
		UARTprintf("getFilterStatusAt: filter-index out of bounds: '%d'", s);
		return -1;
	}
	s--;
	skipWS();
	if (UARTpeek() == '-') {
		UARTgetc();
//		e = min(parseInteger(),MAX_CHAIN_LENGTH) - 1;
		e = parseInteger();
		if (e > MAX_CHAIN_LENGTH)
			e = MAX_CHAIN_LENGTH;
		e--;
		skipWS();

	} else
		e = s;
	if (!UARTexpect("setFilterStatusAt", ','))
		return -1;
	long status = parseInteger();
	skipWS();
	if (UARTpeek() == ',') {
		UARTgetc();
		j = status;
		if (j < 0 || j > 2) {
			UARTprintf(
					"getFilterStatusAt: sub-filter-index out of bounds: '%d'",
					j);
			return -1;
		}
		status = parseInteger();
	}
	for (i = s; i <= e; i++)
		switch (j) {
		case 0:
			if (status & 1)
				filterMixerConfig[i] |= CONFIG_FILTER_1_ENABLE;
			else
				filterMixerConfig[i] &= ~CONFIG_FILTER_1_ENABLE;
			if (status & 2)
				filterMixerConfig[i] |= CONFIG_FILTER_2_ENABLE;
			else
				filterMixerConfig[i] &= ~CONFIG_FILTER_2_ENABLE;
			break;
		case 1:
			if (status)
				filterMixerConfig[i] |= CONFIG_FILTER_1_ENABLE;
			else
				filterMixerConfig[i] &= ~CONFIG_FILTER_1_ENABLE;
			break;
		case 2:
			if (status)
				filterMixerConfig[i] |= CONFIG_FILTER_2_ENABLE;
			else
				filterMixerConfig[i] &= ~CONFIG_FILTER_2_ENABLE;
			break;
		}
#ifdef USE_MFB_FILTERING
	reconfigureMfbChain();
#else
	adjustChainLength();
#endif
	return 0;
}

int computeChainTapCount(int i) {
	int r = 0, j;
	for (j = 0; j < MAX_CHAIN_LENGTH; j++) {
		switch (i) {
		case 0:
			if (filterMixerConfig[j] & CONFIG_FILTER_1_ENABLE)
				r += filterMixerConfig[j] & CONFIG_FILTER_1_SINGLE ? 1 : 2;
			break;
		case 1:
			if (filterMixerConfig[j] & CONFIG_FILTER_2_ENABLE)
				r += filterMixerConfig[j] & CONFIG_FILTER_2_SINGLE ? 1 : 2;
			break;
		}
	}
	return r;
}

long getChainTapCount() {
	long i = parseInteger();
	if (i < 1 || i > 2) {
		UARTprintf("getChainTapCount: chain-index out of bounds: '%d'", i);
		return -1;
	}
	return computeChainTapCount(i - 1);
}

long getChainTapCounts() {
	UARTprintf("{%d,%d}", computeChainTapCount(0), computeChainTapCount(1));
	return 0;
}
void UARTwriteFilterAt(int i, int j) {
	char buf[16];
	floatToString(filterChain[i][j].a0, buf, sizeof(buf));
	UARTprintf("{{%s,", buf);
	floatToString(filterChain[i][j].a1, buf, sizeof(buf));
	UARTprintf("%s", buf);
	if ((filterMixerConfig[i]
			& (j == 1 ? CONFIG_FILTER_2_SINGLE : CONFIG_FILTER_1_SINGLE))
			== 0) {
		floatToString(filterChain[i][j].a2, buf, sizeof(buf));
		UARTprintf(",%s", buf);
	}
	UARTprintf("}");
	if ((filterMixerConfig[i]
			& (j == 1 ? CONFIG_FILTER_2_FIR : CONFIG_FILTER_1_FIR)) == 0) {
		floatToString(filterChain[i][j].b1, buf, sizeof(buf));
		UARTprintf(",{%s", buf);
		if ((filterMixerConfig[i]
				& (j == 1 ? CONFIG_FILTER_2_SINGLE : CONFIG_FILTER_1_SINGLE))
				== 0) {
			floatToString(filterChain[i][j].b2, buf, sizeof(buf));
			UARTprintf(",%s", buf);
		}
		UARTprintf("}");
	}
	UARTprintf("}");
}
long getFilterAt() {
	long i = parseInteger(), j = 0;
	if (i < 1 || i > MAX_CHAIN_LENGTH) {
		UARTprintf("getFilterAt: filter-index out of bounds: '%d'", i);
		return -1;
	}
	i--;
	skipWS();
	if (UARTpeek() == ',') {
		UARTgetc();
		j = parseInteger();
		if (j < 0 || j > 2) {
			UARTprintf("getFilterAt: sub-filter-index out of bounds: '%d'", j);
			return -1;
		}
	}
	if (j == 0)
		UARTprintf("{");
	if (j == 0 || j == 1)
		UARTwriteFilterAt(i, 0);
	if (j == 0)
		UARTprintf(",");
	if (j == 0 || j == 2)
		UARTwriteFilterAt(i, 1);
	if (j == 0)
		UARTprintf("}");
	return 0;
}

int UARTreadFilterAt(int i, int j) {
	filterChain[i][j].a0 = 0.;
	filterChain[i][j].a1 = 0.;
	filterChain[i][j].a2 = 0.;
	filterChain[i][j].b1 = 0.;
	filterChain[i][j].b2 = 0.;
	filterMixerConfig[i] |= (
			j == 1 ?
					CONFIG_FILTER_2_FIR | CONFIG_FILTER_2_SINGLE :
					CONFIG_FILTER_1_FIR | CONFIG_FILTER_1_SINGLE);
	skipWS();
	if (!UARTexpect("UARTreadFilterAt", '{'))
		return -1;
	skipWS();
	if (UARTpeek() != '}') {
		if (!UARTexpect("UARTreadFilterAt", '{'))
			return -1;
		skipWS();
		if (UARTpeek() != '}') {
			filterChain[i][j].a0 = parseFloat();
			skipWS();
			if (UARTpeek() == ',') {
				UARTgetc();
				filterChain[i][j].a1 = parseFloat();
				skipWS();
				if (UARTpeek() == ',') {
					UARTgetc();
					filterChain[i][j].a2 = parseFloat();
					if (filterChain[i][j].a2 != 0.)
						filterMixerConfig[i] &= ~(
								j == 1 ?
										CONFIG_FILTER_2_SINGLE :
										CONFIG_FILTER_1_SINGLE);
					skipWS();
				}
			}
		}
		if (!UARTexpect("UARTreadFilterAt", '}'))
			return -1;
		skipWS();
		if (UARTpeek() == ',') {
			UARTgetc();
			skipWS();
			if (!UARTexpect("UARTreadFilterAt", '{'))
				return -1;
			skipWS();
			if (UARTpeek() != '}') {
				filterChain[i][j].b1 = parseFloat();
				if (filterChain[i][j].b1 != 0.)
					filterMixerConfig[i] &= ~(
							j == 1 ? CONFIG_FILTER_2_FIR : CONFIG_FILTER_1_FIR);
				skipWS();
				if (UARTpeek() == ',') {
					UARTgetc();
					filterChain[i][j].b2 = parseFloat();
					if (filterChain[i][j].b2 != 0.)
						filterMixerConfig[i] &= ~(
								j == 1 ?
										CONFIG_FILTER_2_FIR
												| CONFIG_FILTER_2_SINGLE :
										CONFIG_FILTER_1_FIR
												| CONFIG_FILTER_1_SINGLE);
					skipWS();
				}
			}
			if (!UARTexpect("UARTreadFilterAt", '}'))
				return -1;
			skipWS();
		}
	}
	if (!UARTexpect("UARTreadFilterAt", '}'))
		return -1;
	return 0;
}
long setFilterAt() {
	long i = parseInteger(), j = 0;
	if (i < 1 || i > MAX_CHAIN_LENGTH) {
		UARTprintf("setFilterAt: filter-index out of bounds: '%d'", i);
		return -1;
	}
	i--;
	skipWS();
	if (!UARTexpect("setFilterAt", ','))
		return -1;
	skipWS();
	if (isDigit(UARTpeek())) {
		j = parseInteger();
		if (j < 0 || j > 2) {
			UARTprintf("setFilterAt: sub-filter-index out of bounds: '%d'", j);
			return -1;
		}
		skipWS();
		if (!UARTexpect("setFilterAt", ','))
			return -1;
	}
	if ((j == 0 || j == 1) && UARTreadFilterAt(i, 0) < 0)
		return -1;
	if (j == 0 && !UARTexpect("setFilterAt", ','))
		return -1;
	if ((j == 0 || j == 2) && UARTreadFilterAt(i, 1) < 0)
		return -1;
#ifdef USE_MFB_FILTERING
	reconfigureMfbChain();
#endif
	return 0;
}
long getMaxChainLength() {
	return MAX_CHAIN_LENGTH;
}
