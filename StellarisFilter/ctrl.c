/*
 * ctrl.c
 *
 *  Created on: 04.04.2013
 *      Author: rj
 */
#include <math.h>
#include "ctrl.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "utils/uartstdio.c"

tBoolean UARTexpect(char *proc, char expected) {
	if (UARTpeek() != expected) {
		UARTprintf("%s: expected '%c', got '%c'\n", proc, expected, UARTpeek());
		return false;
	} else {
		UARTgetc();
		return true;
	}
}
tBoolean isWhiteSpace(unsigned char c) {
	switch (c) {
	case ' ':
	case '\t':
		return true;
	}
	return false;
}
tBoolean isWhiteSpaceOrLineFeed(unsigned char c) {
	switch (c) {
	case ' ':
	case '\t':
	case '\r':
	case '\n':
		return true;
	}
	return false;
}
tBoolean isAlpha(unsigned char c) {
	return (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z');
}
tBoolean isDigit(unsigned char c) {
	return c >= '0' && c <= '9';
}
tBoolean isAlphaNumeric(unsigned char c) {
	return isAlpha(c) || isDigit(c);
}
tBoolean isIdentifierPart(unsigned char c) {
	return isAlphaNumeric(c) || c == '_';
}
tBoolean isIdentifierStart(unsigned char c) {
	return isAlpha(c) || c == '_';
}

unsigned long skipWS() {
	unsigned char c = UARTpeek();
	unsigned long t = 0;
	while (isWhiteSpace(c)) {
		UARTgetc();
		c = UARTpeek();
		t++;
	}
	return t;
}
int strcmp(char *x, char *y) {
	if (x == 0)
		return y == 0 ? 0 : -1;
	if (y == 0)
		return 1;
	while (*x != '\0' || *y != '\0') {
		if (*x != *y)
			return *x - *y;
		x++;
		y++;
	}
	return 0;
}
int parseIdentifier(char *id, int maxLen) {
	if (maxLen < 1)
		return -1;
	maxLen--;
	id[0] = UARTpeek();
	if (!isIdentifierStart(id[0])) {
		id[0] = '\0';
		return 0;
	}
	UARTgetc();
	int i;
	for (i = 1; i < maxLen; i++) {
		id[i] = UARTpeek();
		if (!isIdentifierPart(id[i]))
			break;
		UARTgetc();
	}
	id[i] = '\0';
	if (i == maxLen && isIdentifierPart(UARTpeek())) {
		UARTprintf(
				"parseIdentifier: identifier too long: using %s, discarding ",
				id);
		while (isIdentifierPart(UARTpeek()))
			UARTprintf("%c", UARTgetc());
		UARTprintf("\n");
	}
	return i;
}
const MappedVarEntry *mapVariable(char *varName) {
	int i = 0;
	for (; i < MAPPED_VARIABLES_COUNT; i++)
		if (strcmp(varName, mappedVariables[i].name) == 0) {
			__asm("cpsie i");
			return &mappedVariables[i];
		}
	return NULL ;
}
const MappedFuncEntry *mapFunction(char *funcName) {
	int i = 0;
	for (; i < MAPPED_FUNCTIONS_COUNT; i++)
		if (strcmp(funcName, mappedFunctions[i].name) == 0)
			return &mappedFunctions[i];
	return NULL ;
}

void skipCommand() {
	while (UARTgetc() != '\r')
		;
}
long parseInteger() {
	long x = 0;
	tBoolean negative = false;
	skipWS();
	char c = UARTpeek();
	if (c == '-') {
		UARTgetc();
		negative = true;
		c = UARTpeek();
	}
	if (!isDigit(c)) {
		UARTprintf("parseInteger: integer expected, got '%c', assuming 0\n", c);
		return 0;
	}
	while (isDigit(c)) {
		UARTgetc();
		x = x * 10 + (long) (c - '0');
		c = UARTpeek();
	}
	return negative ? -x : x;
}
int decimalExponent(unsigned long x) {
	int r = -1;
	while (x > 0) {
		x /= 10;
		r++;
	}
	return r;
}
int unsignedIntegerToPaddedString(unsigned long x, char *buf, int bufLen,
		int padLen, char padChar) {
	if (bufLen < 2)
		return -1;
	int exp = x > 0 ? decimalExponent(x) : 0;
	if (exp + 2 > bufLen)
		return -1;
	if (padLen < exp + 1)
		padLen = exp + 1;
	if (padLen >= bufLen)
		padLen = bufLen - 1;
	int i = padLen;
	buf[i--] = '\0';
	exp = padLen - exp - 1;
	while (i >= exp) {
		buf[i--] = (char) (x % 10) + '0';
		x /= 10;
	}
	while (i >= 0)
		buf[i--] = padChar;
	return padLen;
}
int unsignedIntegerToString(unsigned long x, char *buf, int bufLen) {
	return unsignedIntegerToPaddedString(x, buf, bufLen, 0, '0');
}
int integerToString(long x, char *buf, int bufLen) {
	if (x < 0) {
		if (bufLen < 3)
			return -1;
		int r = unsignedIntegerToString(-x, buf + 1, bufLen - 1);
		if (r < 0)
			return r;
		buf[0] = '-';
		return r + 1;
	} else
		return unsignedIntegerToString(x, buf, bufLen);
}
float parseFloat() {
	float x = 0;
	tBoolean negative = false, expNegative = false;
	int drp = 0, exp = 0;
	skipWS();
	char c = UARTpeek();
	if (c == '-') {
		UARTgetc();
		negative = true;
		c = UARTpeek();
	}
	while (isDigit(c)) {
		UARTgetc();
		x = x * 10 + (float) (c - '0');
		c = UARTpeek();
	}
	if (c == '.') {
		UARTgetc();
		c = UARTpeek();
		while (isDigit(c)) {
			UARTgetc();
			drp++;
			x = x * 10 + (float) (c - '0');
			c = UARTpeek();
		}
	}
	if (c == 'e' || c == 'E') {
		UARTgetc();
		c = UARTpeek();
		if (c == '-') {
			UARTgetc();
			expNegative = true;
			c = UARTpeek();
		}
		while (isDigit(c)) {
			UARTgetc();
			exp = exp * 10 + (int) (c - '0');
			c = UARTpeek();
		}
	}
	drp += expNegative ? exp : -exp;
	while (drp-- > 0)
		x /= 10;
	while (++drp < 0)
		x *= 10;
	return negative ? -x : x;
}
int decimalFloatExp(float x) {
	if (x == 0.)
		return -46;
	if (x < 0)
		x = -x;
	int r = 0;
	while (x < 1.) {
		x *= 10.;
		r--;
	}
	while (x >= 10.) {
		x /= 10.;
		r++;
	}
	return r;
}
float fastPow(float x, long n) {
	if (n == 0)
		return 1.;
	if (n < 0)
		return 1. / fastPow(x, -n);
	int i = 30;
	while ((n & (1 << i)) == 0)
		i--;
	float y = x;
	while (--i >= 0) {
		y *= y;
		if (n & (1 << i))
			y *= x;
	}
	return y;
}
int floatToString(float x, char *buf, int bufLen) {
	if (bufLen < 2)
		return -1;
	if (x == 0.) {
		if (bufLen == 2) {
			buf[0] = '0';
			buf[1] = '\0';
			return 2;
		} else {
			buf[0] = '0';
			buf[1] = '.';
			buf[2] = '\0';
			return 2;
		}
	}
	if (x < 0.) {
		if (bufLen < 3)
			return -1;
		*buf++ = '-';
		bufLen--;
		x = -x;
	}
	int i = 0, exp = decimalFloatExp(x);
	if (exp <= 7 && exp >= -3) {
		i = unsignedIntegerToString((long) x, buf, bufLen);
		if (i <= 0 || i + 2 > bufLen)
			return i;
		buf[i++] = '.';
		if (exp < 7 && i + 1 < bufLen) {
			if (x != 0.) {
				if (x > 1.)
					x = fmod(x, 1);
				else if (x == 1.)
					x = 0.;
				x *= fastPow(10., min(bufLen - i - 1, 7 - exp));
			}
			i += unsignedIntegerToPaddedString((long) x, buf + i, bufLen - i,
					7 - exp, '0');
		}
	} else {
		int expLen = abs(exp) < 10 ? 1 : 2;
		if (exp < 0)
			expLen++;
		if (bufLen < expLen + 3)
			return -1;
		if (exp < -38) {
			x *= fastPow(10., -exp - 38); // normalize first
			x *= fastPow(10., 38);
		} else
			x *= fastPow(10., -exp);
		buf[i++] = (char) ((long) x) + '0';
		if (bufLen > expLen + 3) {
			buf[i++] = '.';
			if (x != 0.) {
				if (x > 1.)
					x = fmod(x, 1);
				else if (x == 1.)
					x = 0.;
				x *= fastPow(10., min(bufLen - expLen - 4, min(7, exp + 46)));
			}
			i += unsignedIntegerToPaddedString((long) x, buf + i, bufLen - i,
					min(7, exp + 46), '0');
		}
		buf[i++] = 'e';
		i += integerToString(exp, buf + i, bufLen - i);
	}
	buf[i] = '\0';
	return i;
}

int parseFloatList(float *f, int maxLen, tBoolean braces) {
	if (maxLen < 0) {
		UARTprintf("parseFloatList: maxLen is negative: %d\n", maxLen);
		return maxLen;
	}
	skipWS();
	if (braces) {
		if (!UARTexpect("parseFloatList", '{'))
			return -1;
		skipWS();
	}
	int i = 0;
	while (i < maxLen) {
		char c = UARTpeek();
		if (isDigit(c) || c == '.' || c == '-') {
			f[i++] = parseFloat();
			skipWS();
			if (UARTpeek() != ',' || i == maxLen)
				break;
			UARTgetc();
			skipWS();
		} else
			break;
	}
	if (braces) {
		if (!UARTexpect("parseFloatList", '}'))
			return -1;
		skipWS();
	}
	return i;
}
void printMappedVarValue(const MappedVarEntry *var) {
	char buf[16];
	switch (var->flags & VAR_FLAG_TYPE_MASK) {
	case VAR_FLAG_TYPE_CHAR:
		UARTprintf("%c", *(char*) var->var);
		break;
	case VAR_FLAG_TYPE_STRING:
		UARTprintf("%s", (char*) var->var);
		break;
	case VAR_FLAG_TYPE_UINT:
		UARTprintf("%u", *(unsigned int*) var->var);
		break;
	case VAR_FLAG_TYPE_INT:
		UARTprintf("%d", *(int*) var->var);
		break;
	case VAR_FLAG_TYPE_ULONG:
		UARTprintf("%u", *(unsigned long*) var->var);
		break;
	case VAR_FLAG_TYPE_LONG:
		UARTprintf("%d", *(long*) var->var);
		break;
	case VAR_FLAG_TYPE_FLOAT:
//				UARTprintf("%d.%03d\n", (long) *(float*) var->var,
//						(long) (1000 * fmod(*(float*) var->var, 1)));
		if (floatToString(*(float*) var->var, buf, sizeof(buf)) > 0)
			UARTprintf("%s", buf);
		break;
	default:
		UARTprintf("???");
	}
}
void readVar(char *id) {
	const MappedVarEntry *var = mapVariable(id);
	if (var != NULL) {
		if (var->flags & VAR_FLAG_ACCESS_READ)
			printMappedVarValue(mapVariable(id));
		else
			UARTprintf("readVar: variable is not readable: %s", id);
	} else
		UARTprintf("readVar: unknown variable: %s", id);
}

void setVar(char *id) {
	union {
		unsigned int tUInt;
		int tInt;
		unsigned long tULong;
		long tLong;
		float tFloat;
	} t, *target;
	const MappedVarEntry *var = mapVariable(id);
	if (var != NULL) {
		if (var->flags & VAR_FLAG_ACCESS_WRITE) {
			switch (var->flags & VAR_FLAG_TYPE_MASK) {
			case VAR_FLAG_TYPE_UINT:
				t.tUInt = (unsigned int) parseInteger();
				if (var->flags & VAR_FLAG_HAS_CHANGE_HOOK)
					var->changeHook(var->var, &t.tUInt);
				else
					*(unsigned int*) var->var = t.tUInt;
				break;
			case VAR_FLAG_TYPE_INT:
				t.tInt = parseInteger();
				if (var->flags & VAR_FLAG_HAS_CHANGE_HOOK)
					var->changeHook(var->var, &t.tInt);
				else
					*(int*) var->var = t.tInt;
				break;
			case VAR_FLAG_TYPE_ULONG:
				t.tULong = (unsigned long) parseInteger();
				if (var->flags & VAR_FLAG_HAS_CHANGE_HOOK)
					var->changeHook(var->var, &t.tULong);
				else
					*(unsigned long*) var->var = t.tULong;
				break;
			case VAR_FLAG_TYPE_LONG:
				t.tLong = (unsigned long) parseInteger();
				if (var->flags & VAR_FLAG_HAS_CHANGE_HOOK)
					var->changeHook(var->var, &t.tLong);
				else
					*(unsigned long*) var->var = t.tULong;
				break;
			case VAR_FLAG_TYPE_FLOAT:
				t.tFloat = parseFloat();
				if (var->flags & VAR_FLAG_HAS_CHANGE_HOOK)
					var->changeHook(var->var, &t.tFloat);
				else
					*(float*) var->var = t.tFloat;
				break;
			case VAR_FLAG_TYPE_CHAR:
			case VAR_FLAG_TYPE_STRING:
			default:
				UARTprintf("setVar: unsupported type of var: %s", id);
			}
		} else
			UARTprintf("setVar: variable is not writable: %s", id);
	} else
		UARTprintf("setVar: unknown variable: %s", id);
}
long invokeFunction(char *id) {
	skipWS();
	if (!UARTexpect("invokeFunction", '('))
		return 0;
	const MappedFuncEntry *func = mapFunction(id);
	if (func == NULL) {
		UARTprintf("invokeFunction: unknown function: %s", id);
		return 0;
	}
	long r = func->f();
	if ((func->flags & FUNC_FLAG_INTEGER_RETURN)
			&& (r >= 0
					|| (func->flags & FUNC_FLAG_NEGATIVE_RETURN_INDICATES_ERROR)
							== 0))
		UARTprintf("%d", r);
	skipWS();
	UARTexpect("invokeFunction", ')');
	return r;
}
long listVariables() {
	int i;
	for (i = 0; i < MAPPED_VARIABLES_COUNT; i++) {
		UARTprintf("%s,", mappedVariables[i].name);
		switch (mappedVariables[i].flags & VAR_FLAG_ACCESS_MASK) {
		case VAR_FLAG_ACCESS_READ:
			UARTprintf("ro");
			break;
		case VAR_FLAG_ACCESS_WRITE:
			UARTprintf("wo");
			break;
		case VAR_FLAG_ACCESS_RW:
			UARTprintf("rw");
			break;
		default:
			UARTprintf("na");
		}
		UARTprintf(",");
		switch (mappedVariables[i].flags & VAR_FLAG_TYPE_MASK) {
		case VAR_FLAG_TYPE_CHAR:
			UARTprintf("char");
			break;
		case VAR_FLAG_TYPE_STRING:
			UARTprintf("char*");
			break;
		case VAR_FLAG_TYPE_INT:
			UARTprintf("int");
			break;
		case VAR_FLAG_TYPE_UINT:
			UARTprintf("unsigned int");
			break;
		case VAR_FLAG_TYPE_LONG:
			UARTprintf("long");
			break;
		case VAR_FLAG_TYPE_ULONG:
			UARTprintf("unsigned long");
			break;
		case VAR_FLAG_TYPE_FLOAT:
			UARTprintf("float");
			break;
		case VAR_FLAG_TYPE_UNDEFINED:
			UARTprintf("???");
			break;
		}
		UARTprintf(",");
		printMappedVarValue(&mappedVariables[i]);
		if (mappedVariables[i].desc)
			UARTprintf(": %s", mappedVariables[i].desc);
		UARTprintf("\n");
	}
	return i;
}
long listFunctions() {
	int i;
	for (i = 0; i < MAPPED_FUNCTIONS_COUNT; i++) {
		UARTprintf("%s(", mappedFunctions[i].name);
		if (mappedFunctions[i].paramList)
			UARTprintf("%s", mappedFunctions[i].paramList);
		UARTprintf(")");
		if (mappedFunctions[i].desc)
			UARTprintf(": %s", mappedFunctions[i].desc);
		UARTprintf("\n");
	}
	return i;
}
void parseCommand() {
	char id[MAX_VAR_NAME_LENGTH + 1];
	long r = parseIdentifier(id, MAX_VAR_NAME_LENGTH + 1);
	if (r <= 0) {
		UARTprintf("parseCommand: identifier expected\n");
		skipCommand();
		return;
	}
	skipWS();
	char c = UARTpeek();
	switch (c) {
	case '\r':
	case '\n':
		UARTgetc();
		readVar(id);
		break;
	case '=':
		UARTgetc();
		setVar(id);
		skipCommand();
		break;
	case '(':
		invokeFunction(id);
		skipCommand();
		break;
	}
}

void initUART() {
	MAP_SysCtlPeripheralEnable (SYSCTL_PERIPH_GPIOA);
	MAP_GPIOPinConfigure (GPIO_PA0_U0RX);
	MAP_GPIOPinConfigure (GPIO_PA1_U0TX);
	MAP_GPIOPinTypeUART (GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);
	UARTStdioInit(0);
}
//char cmd[128];
void loopUART_cmd() {
	while (1) {
		UARTprintf("\n>");
		parseCommand();
	}
}

