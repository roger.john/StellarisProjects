/*
 * filter.h
 *
 *  Created on: 04.04.2013
 *      Author: rj
 */

#ifndef FILTER_H_
#define FILTER_H_

#include "inc/hw_types.h"

#define MAX_CHAIN_LENGTH       36
#define CONFIG_MIX              1
#define CONFIG_FILTER_1_ENABLE  2
#define CONFIG_FILTER_1_FIR     4
#define CONFIG_FILTER_1_SINGLE  8
#define CONFIG_FILTER_2_ENABLE 16
#define CONFIG_FILTER_2_FIR    32
#define CONFIG_FILTER_2_SINGLE 64

typedef struct {
	float a0, x1, a1, x2, a2;
	float y1, b1, y2, b2;
} iir_biquad;
typedef struct {
	float m[2][2];
} mixer;

float filterBIIR(iir_biquad *f, float x, tBoolean clockParity);
float filterBIIR_evenClock(iir_biquad *f, float x);
float filterBIIR_oddClock(iir_biquad *f, float x);
float filterSIIR(iir_biquad *f, float x, tBoolean clockParity);
float filterSIIR_evenClock(iir_biquad *f, float x);
float filterSIIR_oddClock(iir_biquad *f, float x);
float filterBFIR(iir_biquad *f, float x, tBoolean clockParity);
float filterBFIR_evenClock(iir_biquad *f, float x);
float filterBFIR_oddClock(iir_biquad *f, float x);
float filterSFIR(iir_biquad *f, float x, tBoolean clockParity);
float filterSFIR_evenClock(iir_biquad *f, float x);
float filterSFIR_oddClock(iir_biquad *f, float x);


void mix(mixer *m, float *x1, float *x2);

void initFiltering(void);
void filter(float *f1, float *f2,unsigned long clockTick);

long getMixerAt();
long getMixerStatusAt();
long setMixerAt();
long setMixerStatusAt();
long getFilterAt();
long getFilterStatusAt();
long setFilterAt();
long setFilterStatusAt();
long getChainTapCount();
long getChainTapCounts();
long getMaxChainLength();

#endif /* FILTER_H_ */
