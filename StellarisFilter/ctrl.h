/*
 * ctrl.h
 *
 *  Created on: 04.04.2013
 *      Author: rj
 */

#ifndef CTRL_H_
#define CTRL_H_

#include "inc/hw_gpio.h"
#include "inc/hw_types.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "main.h"
#include "io.h"
#include "filter.h"

#define MAX_VAR_NAME_LENGTH 32
#define VAR_FLAG_TYPE_MASK       0x7
#define VAR_FLAG_TYPE_CHAR       0
#define VAR_FLAG_TYPE_STRING     1
#define VAR_FLAG_TYPE_INT        2
#define VAR_FLAG_TYPE_UINT       3
#define VAR_FLAG_TYPE_LONG       4
#define VAR_FLAG_TYPE_ULONG      5
#define VAR_FLAG_TYPE_FLOAT      6
#define VAR_FLAG_TYPE_UNDEFINED  7
#define VAR_FLAG_ACCESS_MASK     0x18
#define VAR_FLAG_ACCESS_READ     0x08
#define VAR_FLAG_ACCESS_WRITE    0x10
#define VAR_FLAG_ACCESS_RW (VAR_FLAG_ACCESS_READ|VAR_FLAG_ACCESS_WRITE)
#define VAR_FLAG_HAS_CHANGE_HOOK 0x20
#define FUNC_FLAG_INTEGER_RETURN 1
#define FUNC_FLAG_NEGATIVE_RETURN_INDICATES_ERROR 2

typedef struct {
	char *name;
	void *var;
	unsigned long flags;
	void (*changeHook)(void *var,void *newVal);
	char *desc;
} MappedVarEntry;
typedef struct {
	char *name;
	long (*f)();
	unsigned long flags;
	char *paramList;
	char *desc;
} MappedFuncEntry;

extern unsigned long samplePeriod;
extern volatile unsigned long tick;
extern float halfSamplePeriod, frequency,x1,x2,
	pwmCenterDutyA,pwmCenterDutyB,adcCenterValA,adcCenterValB;
extern float auxFreqA,auxDutyA,auxFreqB,auxDutyB;
extern long dutyA, dutyB, adcA, adcB;
extern enum Mode mode;
extern enum Error lastError;
#ifdef PROFILE
extern unsigned long mainStartClock,adcIsrCounts[6], adcIsrStartClocks[5];
extern long mainClocks, adcIsrClocks[5];
#endif
long listVariables();
long listFunctions();

static const MappedVarEntry mappedVariables[] = {
		{ "samplePeriod", &samplePeriod, VAR_FLAG_TYPE_ULONG | VAR_FLAG_ACCESS_READ },
		{ "tick", &tick, VAR_FLAG_TYPE_ULONG | VAR_FLAG_ACCESS_READ },
#ifdef PROFILE
		{ "adcIsrCount", &adcIsrCounts[5], VAR_FLAG_TYPE_ULONG | VAR_FLAG_ACCESS_READ },
		{ "adcIsrCount0", &adcIsrCounts[0], VAR_FLAG_TYPE_ULONG | VAR_FLAG_ACCESS_READ },
		{ "adcIsrCount1", &adcIsrCounts[1], VAR_FLAG_TYPE_ULONG | VAR_FLAG_ACCESS_READ },
		{ "adcIsrCount2", &adcIsrCounts[2], VAR_FLAG_TYPE_ULONG | VAR_FLAG_ACCESS_READ },
		{ "adcIsrCount3", &adcIsrCounts[3], VAR_FLAG_TYPE_ULONG | VAR_FLAG_ACCESS_READ },
		{ "adcIsrCount4", &adcIsrCounts[4], VAR_FLAG_TYPE_ULONG | VAR_FLAG_ACCESS_READ },
		{ "adcIsrStartClock0", &adcIsrStartClocks[0], VAR_FLAG_TYPE_ULONG | VAR_FLAG_ACCESS_READ },
		{ "adcIsrStartClock1", &adcIsrStartClocks[1], VAR_FLAG_TYPE_ULONG | VAR_FLAG_ACCESS_READ },
		{ "adcIsrStartClock2", &adcIsrStartClocks[2], VAR_FLAG_TYPE_ULONG | VAR_FLAG_ACCESS_READ },
		{ "adcIsrStartClock3", &adcIsrStartClocks[3], VAR_FLAG_TYPE_ULONG | VAR_FLAG_ACCESS_READ },
		{ "adcIsrStartClock4", &adcIsrStartClocks[4], VAR_FLAG_TYPE_ULONG | VAR_FLAG_ACCESS_READ },
		{ "adcIsrClocks0", &adcIsrClocks[0], VAR_FLAG_TYPE_LONG | VAR_FLAG_ACCESS_READ },
		{ "adcIsrClocks1", &adcIsrClocks[1], VAR_FLAG_TYPE_LONG | VAR_FLAG_ACCESS_READ },
		{ "adcIsrClocks2", &adcIsrClocks[2], VAR_FLAG_TYPE_LONG | VAR_FLAG_ACCESS_READ },
		{ "adcIsrClocks3", &adcIsrClocks[3], VAR_FLAG_TYPE_LONG | VAR_FLAG_ACCESS_READ },
		{ "adcIsrClocks4", &adcIsrClocks[4], VAR_FLAG_TYPE_LONG | VAR_FLAG_ACCESS_READ },
		{ "mainStartClock", &mainStartClock, VAR_FLAG_TYPE_ULONG | VAR_FLAG_ACCESS_READ },
		{ "mainClocks", &mainClocks, VAR_FLAG_TYPE_LONG | VAR_FLAG_ACCESS_READ },
#endif
		{ "halfSamplePeriod", &halfSamplePeriod, VAR_FLAG_TYPE_FLOAT | VAR_FLAG_ACCESS_READ },
		{ "pwmCenterDutyA", &pwmCenterDutyA, VAR_FLAG_TYPE_FLOAT | VAR_FLAG_ACCESS_RW },
		{ "pwmCenterDutyB", &pwmCenterDutyB, VAR_FLAG_TYPE_FLOAT | VAR_FLAG_ACCESS_RW },
		{ "adcCenterValA", &adcCenterValA, VAR_FLAG_TYPE_FLOAT | VAR_FLAG_ACCESS_RW },
		{ "adcCenterValB", &adcCenterValB, VAR_FLAG_TYPE_FLOAT | VAR_FLAG_ACCESS_RW },
		{ "dutyA", &dutyA, VAR_FLAG_TYPE_LONG | VAR_FLAG_ACCESS_RW },
		{ "dutyB", &dutyB, VAR_FLAG_TYPE_LONG | VAR_FLAG_ACCESS_RW },
		{ "adcA", &adcA, VAR_FLAG_TYPE_LONG | VAR_FLAG_ACCESS_RW },
		{ "adcB", &adcB, VAR_FLAG_TYPE_LONG | VAR_FLAG_ACCESS_RW },
		{ "x1", &x1, VAR_FLAG_TYPE_FLOAT | VAR_FLAG_ACCESS_RW },
		{ "x2", &x2, VAR_FLAG_TYPE_FLOAT | VAR_FLAG_ACCESS_RW },
		{ "mode", &mode, VAR_FLAG_TYPE_ULONG | VAR_FLAG_ACCESS_RW },
		{ "lastError", &lastError, VAR_FLAG_TYPE_ULONG | VAR_FLAG_ACCESS_RW },
		{ "frequency", &frequency, VAR_FLAG_TYPE_FLOAT | VAR_FLAG_ACCESS_RW },
		{ "auxFreqA", &auxFreqA, VAR_FLAG_TYPE_FLOAT | VAR_FLAG_ACCESS_RW | VAR_FLAG_HAS_CHANGE_HOOK, updateAUXTimerValue },
		{ "auxDutyA", &auxDutyA, VAR_FLAG_TYPE_FLOAT | VAR_FLAG_ACCESS_RW | VAR_FLAG_HAS_CHANGE_HOOK, updateAUXTimerValue  },
		{ "auxFreqB", &auxFreqB, VAR_FLAG_TYPE_FLOAT | VAR_FLAG_ACCESS_RW | VAR_FLAG_HAS_CHANGE_HOOK, updateAUXTimerValue  },
		{ "auxDutyB", &auxDutyB, VAR_FLAG_TYPE_FLOAT | VAR_FLAG_ACCESS_RW | VAR_FLAG_HAS_CHANGE_HOOK, updateAUXTimerValue  },
};
static const MappedFuncEntry mappedFunctions[] = {
		{ "resetADC", resetADC, 0 },
		{ "reviveADC", reviveADC, 0 },
		{ "resetPWM", resetPWM, 0 },
		{ "syncIO",	syncIO, 0 },
		{ "clock", clock, FUNC_FLAG_INTEGER_RETURN },
		{ "syncSysTick", syncSysTick, 0 },
#if SW_AVERAGING > 1
		{ "ioOffset", ioOffset, 0 },
		{ "ioOffsetInClocks", ioOffsetInClocks, FUNC_FLAG_INTEGER_RETURN },
#endif
		{ "outputSkew", outputSkew, 0 },
		{ "outputSkewInClocks", outputSkewInClocks,	FUNC_FLAG_INTEGER_RETURN },
		{ "sysTickOffset", sysTickOffset, 0 },
		{ "sysTickOffsetInClocks", sysTickOffsetInClocks, FUNC_FLAG_INTEGER_RETURN },
		{ "zeroAdjustInputA", zeroAdjustInputA, 0 },
		{ "zeroAdjustInputB", zeroAdjustInputB, 0 },
		{ "getMixerAt", getMixerAt,	FUNC_FLAG_NEGATIVE_RETURN_INDICATES_ERROR },
		{ "getMixerStatusAt", getMixerStatusAt, FUNC_FLAG_INTEGER_RETURN | FUNC_FLAG_NEGATIVE_RETURN_INDICATES_ERROR },
		{ "setMixerAt", setMixerAt,	FUNC_FLAG_NEGATIVE_RETURN_INDICATES_ERROR },
		{ "setMixerStatusAt", setMixerStatusAt,	FUNC_FLAG_NEGATIVE_RETURN_INDICATES_ERROR },
		{ "getFilterStatusAt", getFilterStatusAt, FUNC_FLAG_INTEGER_RETURN | FUNC_FLAG_NEGATIVE_RETURN_INDICATES_ERROR },
		{ "setFilterStatusAt", setFilterStatusAt, FUNC_FLAG_NEGATIVE_RETURN_INDICATES_ERROR },
		{ "getFilterAt",	getFilterAt, FUNC_FLAG_NEGATIVE_RETURN_INDICATES_ERROR },
		{ "setFilterAt", setFilterAt, FUNC_FLAG_NEGATIVE_RETURN_INDICATES_ERROR },
		{ "getChainTapCount", getChainTapCount, FUNC_FLAG_INTEGER_RETURN | FUNC_FLAG_NEGATIVE_RETURN_INDICATES_ERROR },
		{ "getChainTapCounts", getChainTapCounts, FUNC_FLAG_NEGATIVE_RETURN_INDICATES_ERROR },
		{ "getMaxChainLength", getMaxChainLength, FUNC_FLAG_INTEGER_RETURN },
		{ "listVariables", listVariables, FUNC_FLAG_INTEGER_RETURN },
		{ "listFunctions", listFunctions, FUNC_FLAG_INTEGER_RETURN },
};

#define MAPPED_VARIABLES_COUNT  sizeof(mappedVariables)/sizeof(MappedVarEntry)
#define MAPPED_FUNCTIONS_COUNT  sizeof(mappedFunctions)/sizeof(MappedFuncEntry)

void initUART();
void loopUART_cmd();

int integerToString(long x, char *buf, int bufLen);
int floatToString(float x, char *buf, int bufLen);
tBoolean UARTexpect(char *proc, char expected);
unsigned long skipWS();
tBoolean isWhiteSpace(unsigned char c);
tBoolean isWhiteSpaceOrLineFeed(unsigned char c);
tBoolean isAlpha(unsigned char c);
tBoolean isDigit(unsigned char c);
tBoolean isNumeric(unsigned char c);
long parseInteger();
float parseFloat();
int parseFloatList(float *f, int maxLen, tBoolean braces);

#endif /* CTRL_H_ */
