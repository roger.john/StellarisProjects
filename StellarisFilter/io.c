/*
 * io.c
 *
 *  Created on: 07.04.2013
 *      Author: rj
 */
#include "main.h"
#include "inc/hw_gpio.h"
#include "inc/hw_ints.h"
#include "inc/hw_nvic.h"
#include "inc/hw_udma.h"
#include "driverlib/adc.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/timer.h"
#include "driverlib/udma.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "utils/uartstdio.h"
#include "ctrl.h"
#include "io.h"

extern unsigned long samplePeriod;
extern volatile unsigned long tick;
unsigned long adcIsrCounts[6], adcIsrStartClocks[5];
extern float halfSamplePeriod, pwmCenterDutyA, pwmCenterDutyB, adcCenterValA,
		adcCenterValB;
extern long dutyA, dutyB, adcA, adcB;
extern float auxFreqA, auxDutyA, auxFreqB, auxDutyB;
extern enum Error lastError;
extern enum Mode mode;
long adcIsrClocks[5];

void adcIsr(void);
void udmaIsr(void);

#define SYS_TICK_SYNC_OFFSET 14
#define IMPL_SYNC_SYS_TICK(refTimer) \
	long syncSysTick() { \
		__asm("cpsid i"); \
		while(HWREG(refTimer)>SYS_TICK_SYNC_OFFSET) { }\
		HWREG(NVIC_ST_CURRENT) = 0; \
		__asm("cpsie i"); \
		return 0; \
	}

#ifdef SW_AVERAGING

static tDMAControlTable uDMAcontrolTable[2 * 32] __attribute__((aligned(1024)));
static unsigned long savedUDMAcontrolWords[2][2];
static long rawAdcA1[SW_AVERAGING], rawAdcA2[SW_AVERAGING],
		rawAdcB1[SW_AVERAGING], rawAdcB2[SW_AVERAGING];

long syncIO() {
	HWREG(TIMER0_BASE + TIMER_O_SYNC) = PWM_TIMER_A_SYNC | PWM_TIMER_B_SYNC
			| ADC_TIMER_SYNC | REF_TIMER_SYNC;
	HWREG(TIMER0_BASE + TIMER_O_SYNC) = 0; // errata 9.1
	return 0;
}

IMPL_SYNC_SYS_TICK(REF_TIMER_BASE+TIMER_O_TBR)

//long syncSysTick() {
//	__asm("cpsid i");
//	while(HWREG(ADC_TIMER_BASE+TIMER_O_TBR)>72) {
//	}
//	HWREG(NVIC_ST_CURRENT) = 0;
//	__asm("cpsie i");
//	return 0;
//}

#define IMPL_ADC_TO_FLOAT(suffix) \
	inline float adcToFloat##suffix(long adcSample) { \
		return (((float) adcSample) - adcCenterVal##suffix) \
			* (1. / (SW_AVERAGING * 2048.)); \
	}
IMPL_ADC_TO_FLOAT(A)
IMPL_ADC_TO_FLOAT(B)

void initADC(void) {
	MAP_SysCtlPeripheralEnable (SYSCTL_PERIPH_ADC0);
	MAP_SysCtlPeripheralEnable (SYSCTL_PERIPH_ADC1);
	MAP_SysCtlPeripheralEnable (ADC0_PIN_PERIPH);
	MAP_SysCtlPeripheralEnable (ADC1_PIN_PERIPH);
	MAP_SysCtlPeripheralEnable (SYSCTL_PERIPH2_UDMA);
	MAP_SysCtlPeripheralEnable (ADC_TIMER_PERIPH);
//	MAP_SysCtlPeripheralEnable (REF_TIMER_PIN_PERIPH);
	MAP_SysCtlPeripheralEnable (SYSCTL_PERIPH_TIMER0);
	MAP_uDMAEnable ();
	MAP_uDMAControlBaseSet (uDMAcontrolTable);
	MAP_uDMAChannelAssign (UDMA_CH14_ADC0_0);
	MAP_uDMAChannelAssign (UDMA_CH24_ADC1_0);
	MAP_uDMAChannelAttributeEnable (UDMA_CHANNEL_ADC0, UDMA_ATTR_HIGH_PRIORITY);
	MAP_uDMAChannelAttributeDisable (UDMA_CHANNEL_ADC0,
			UDMA_ATTR_ALTSELECT | UDMA_ATTR_USEBURST | UDMA_ATTR_REQMASK);
	MAP_uDMAChannelAttributeEnable (UDMA_SEC_CHANNEL_ADC10,
			UDMA_ATTR_HIGH_PRIORITY);
	MAP_uDMAChannelAttributeDisable (UDMA_SEC_CHANNEL_ADC10,
			UDMA_ATTR_ALTSELECT | UDMA_ATTR_USEBURST | UDMA_ATTR_REQMASK);
	if (ADC0_PIN_CFG)
		MAP_GPIOPinConfigure (ADC0_PIN_CFG);
	if (ADC1_PIN_CFG)
		MAP_GPIOPinConfigure (ADC1_PIN_CFG);
	MAP_GPIOPinTypeADC (ADC0_PIN_BASE, ADC0_PIN);
	MAP_GPIOPinTypeADC (ADC1_PIN_BASE, ADC1_PIN);
	resetADC();
	adcCenterValA = ADC_CENTER_VAL * SW_AVERAGING;
	adcCenterValB = ADC_CENTER_VAL * SW_AVERAGING;
}

long resetADC(void) {
	int i;
	MAP_SysCtlADCSpeedSet (SYSCTL_ADCSPEED_1MSPS);
	MAP_ADCResolutionSet (ADC0_BASE, ADC_RES_12BIT);
	MAP_ADCResolutionSet (ADC1_BASE, ADC_RES_12BIT);
	MAP_ADCHardwareOversampleConfigure (ADC0_BASE, HW_AVERAGING);
	MAP_ADCHardwareOversampleConfigure (ADC1_BASE, HW_AVERAGING);
	for (i = 0; i < 4; i++) {
		MAP_ADCSequenceDisable (ADC0_BASE, i);
		MAP_ADCSequenceDisable (ADC1_BASE, i);
	}
	MAP_ADCSequenceConfigure (ADC0_BASE, 0, ADC_TRIGGER_TIMER, 0);
	MAP_ADCSequenceConfigure (ADC1_BASE, 0, ADC_TRIGGER_TIMER, 0);
	for (i = 0; i < 3; i++) {
		MAP_ADCSequenceStepConfigure (ADC0_BASE, 0, i, ADC0_CH);
		MAP_ADCSequenceStepConfigure (ADC1_BASE, 0, i, ADC1_CH);
	}
	MAP_ADCSequenceStepConfigure (ADC0_BASE, 0, 3, ADC0_CH | ADC_CTL_IE);
	MAP_ADCSequenceStepConfigure (ADC1_BASE, 0, 3, ADC1_CH | ADC_CTL_IE);
	for (i = 4; i < 7; i++) {
		MAP_ADCSequenceStepConfigure (ADC0_BASE, 0, i, ADC0_CH);
		MAP_ADCSequenceStepConfigure (ADC1_BASE, 0, i, ADC1_CH);
	}
	MAP_ADCSequenceStepConfigure (ADC0_BASE, 0, 7,
			ADC0_CH | ADC_CTL_IE | ADC_CTL_END);
	MAP_ADCSequenceStepConfigure (ADC1_BASE, 0, 7,
			ADC1_CH | ADC_CTL_IE | ADC_CTL_END);

	MAP_uDMAChannelDisable (UDMA_CHANNEL_ADC0);
	MAP_uDMAChannelDisable (UDMA_SEC_CHANNEL_ADC10);
	MAP_uDMAChannelControlSet (UDMA_CHANNEL_ADC0 | UDMA_PRI_SELECT,
			UDMA_SIZE_32 | UDMA_ARB_4 | UDMA_SRC_INC_NONE | UDMA_DST_INC_32);
	MAP_uDMAChannelControlSet (UDMA_CHANNEL_ADC0 | UDMA_ALT_SELECT,
			UDMA_SIZE_32 | UDMA_ARB_4 | UDMA_SRC_INC_NONE | UDMA_DST_INC_32);
	MAP_uDMAChannelControlSet (UDMA_SEC_CHANNEL_ADC10 | UDMA_PRI_SELECT,
			UDMA_SIZE_32 | UDMA_ARB_4 | UDMA_SRC_INC_NONE | UDMA_DST_INC_32);
	MAP_uDMAChannelControlSet (UDMA_SEC_CHANNEL_ADC10 | UDMA_ALT_SELECT,
			UDMA_SIZE_32 | UDMA_ARB_4 | UDMA_SRC_INC_NONE | UDMA_DST_INC_32);
	MAP_uDMAChannelTransferSet (UDMA_CHANNEL_ADC0 | UDMA_PRI_SELECT,
			UDMA_MODE_PINGPONG, (void*) (ADC0_BASE + ADC_O_SSFIFO0), rawAdcA1,
			SW_AVERAGING);
	MAP_uDMAChannelTransferSet (UDMA_CHANNEL_ADC0 | UDMA_ALT_SELECT,
			UDMA_MODE_PINGPONG, (void*) (ADC0_BASE + ADC_O_SSFIFO0), rawAdcA2,
			SW_AVERAGING);
	MAP_uDMAChannelTransferSet (UDMA_SEC_CHANNEL_ADC10 | UDMA_PRI_SELECT,
			UDMA_MODE_PINGPONG, (void*) (ADC1_BASE + ADC_O_SSFIFO0), rawAdcB1,
			SW_AVERAGING);
	MAP_uDMAChannelTransferSet (UDMA_SEC_CHANNEL_ADC10 | UDMA_ALT_SELECT,
			UDMA_MODE_PINGPONG, (void*) (ADC1_BASE + ADC_O_SSFIFO0), rawAdcB2,
			SW_AVERAGING);
	savedUDMAcontrolWords[0][0] = uDMAcontrolTable[UDMA_CHANNEL_ADC0].ulControl;
	savedUDMAcontrolWords[0][1] = uDMAcontrolTable[UDMA_CHANNEL_ADC0
			+ UDMA_ALT_SELECT].ulControl;
	savedUDMAcontrolWords[1][0] =
			uDMAcontrolTable[UDMA_SEC_CHANNEL_ADC10].ulControl;
	savedUDMAcontrolWords[1][1] = uDMAcontrolTable[UDMA_SEC_CHANNEL_ADC10
			+ UDMA_ALT_SELECT].ulControl;
	MAP_IntEnable (UDMA_INT_ERR);
	MAP_uDMAChannelEnable (UDMA_CHANNEL_ADC0);
	MAP_uDMAChannelEnable (UDMA_SEC_CHANNEL_ADC10);
	MAP_IntEnable (INT_ADC0SS0);
	MAP_IntEnable (INT_ADC1SS0);
//	ADCIntEnable(ADC0_BASE, 3);
//	ADCIntEnable(ADC1_BASE, 3);
	MAP_TimerDisable (ADC_TIMER_BASE, TIMER_BOTH);
	MAP_ADCSequenceEnable (ADC0_BASE, 0);
	MAP_ADCSequenceEnable (ADC1_BASE, 0);
	MAP_TimerConfigure (ADC_TIMER_BASE,
			TIMER_CFG_SPLIT_PAIR | TIMER_CFG_A_PERIODIC | TIMER_CFG_B_PWM);
	MAP_TimerControlStall (ADC_TIMER_BASE, TIMER_BOTH, true);
	MAP_TimerControlTrigger (ADC_TIMER_BASE, ADC_TIMER, true);
	MAP_GPIOPinConfigure (REF_TIMER_PIN_CFG);
	MAP_GPIOPinTypeTimer (REF_TIMER_PIN_BASE, REF_TIMER_PIN);
	MAP_GPIOPadConfigSet (REF_TIMER_PIN_BASE, REF_TIMER_PIN,
			GPIO_STRENGTH_8MA_SC, GPIO_PIN_TYPE_STD);
	HWREG(ADC_TIMER_BASE + TIMER_O_TBMR) |= (TIMER_TBMR_TBMRSU
			| TIMER_TBMR_TBPLO | TIMER_TBMR_TBILD);
	MAP_TimerLoadSet (ADC_TIMER_BASE, ADC_TIMER,
			samplePeriod / SW_AVERAGING - 1);
	MAP_TimerLoadSet (ADC_TIMER_BASE, REF_TIMER, samplePeriod - 1);
	MAP_TimerMatchSet (ADC_TIMER_BASE, REF_TIMER, samplePeriod / 2 - 1);
	MAP_TimerEnable (ADC_TIMER_BASE, TIMER_BOTH);
	return 0;
}
#define arraysum(a,i,n) (n==1?arraysum1(a,i):\
		n==2?arraysum2(a,i):\
				n==4?arraysum4(a,i):\
						n==8?arraysum8(a,i):\
								n==16?arraysum16(a,i):\
										n==32?arraysum32(a,i):\
												error())
#define arraysum1(a,i) (a[i])
#define arraysum2(a,i) (arraysum1(a,i)+arraysum1(a,i+1))
#define arraysum4(a,i) (arraysum2(a,i)+arraysum2(a,i+2))
#define arraysum8(a,i) (arraysum4(a,i)+arraysum4(a,i+4))
#define arraysum16(a,i) (arraysum8(a,i)+arraysum8(a,i+8))
#define arraysum32(a,i) (arraysum16(a,i)+arraysum16(a,i+16))
void adcIsr(void) {
	__asm("cpsid i");
	//	unsigned long adc0Status = MAP_ADCIntStatus(ADC0_BASE, 0, true), adc1Status =
	//			MAP_ADCIntStatus(ADC1_BASE, 0, true);
#ifdef PROFILE
	unsigned long adcIsrStartClock = HWREG(NVIC_ST_CURRENT);
	adcIsrCounts[5]++;
	int c = 0;
#endif
#ifdef EXT_PROFILE
	GPIO_ENABLE_PIN(EXT_PROFILE_ADC_GPIO_BASE, EXT_PROFILE_ADC_GPIO_PIN);
#endif
	unsigned long uDMAstatus = HWREG(UDMA_CHIS); //MAP_uDMAIntStatus();
	/*int i;*/
	if (uDMAstatus & (1 << UDMA_CHANNEL_ADC0)) {
		HWREG(UDMA_CHIS) = 1 << UDMA_CHANNEL_ADC0; //uDMAIntClear(1 << UDMA_CHANNEL_ADC0);
//		MAP_ADCIntClear(ADC0_BASE, ADC_INT_SS0);
		if ((uDMAcontrolTable[UDMA_CHANNEL_ADC0].ulControl
				& UDMA_CHCTL_XFERMODE_M) == UDMA_CHCTL_XFERMODE_STOP) {
//			adcA = rawAdcA1[0];
//			for (i = 1; i < SW_AVERAGING; i++)
//				adcA += rawAdcA1[i];
			adcA = arraysum(rawAdcA1,0,SW_AVERAGING);
			uDMAcontrolTable[UDMA_CHANNEL_ADC0].ulControl =
					savedUDMAcontrolWords[0][0];
#ifdef PROFILE
			c++;
#endif
		}
		if ((uDMAcontrolTable[UDMA_CHANNEL_ADC0 + UDMA_ALT_SELECT].ulControl
				& UDMA_CHCTL_XFERMODE_M) == UDMA_CHCTL_XFERMODE_STOP) {
//			adcA = rawAdcA2[0];
//			for (i = 1; i < SW_AVERAGING; i++)
//				adcA += rawAdcA2[i];
			adcA = arraysum(rawAdcA2,0,SW_AVERAGING);
			uDMAcontrolTable[UDMA_CHANNEL_ADC0 + UDMA_ALT_SELECT].ulControl =
					savedUDMAcontrolWords[0][1];
#ifdef PROFILE
			c++;
#endif
		}
//		HWREG(UDMA_ENASET) = 1 << UDMA_CHANNEL_ADC0; //MAP_uDMAChannelEnable(UDMA_CHANNEL_ADC0);
	}
	if (uDMAstatus & (1 << UDMA_SEC_CHANNEL_ADC10)) {
		HWREG(UDMA_CHIS) = 1 << UDMA_SEC_CHANNEL_ADC10; //MAP_uDMAIntClear(1 << UDMA_SEC_CHANNEL_ADC10);
//		ADCIntClear(ADC1_BASE, ADC_INT_SS0);
		if ((uDMAcontrolTable[UDMA_SEC_CHANNEL_ADC10].ulControl
				& UDMA_CHCTL_XFERMODE_M) == UDMA_CHCTL_XFERMODE_STOP) {
//			adcB = rawAdcB1[0];
//			for (i = 1; i < SW_AVERAGING; i++)
//				adcB += rawAdcB1[i];
			adcB = arraysum(rawAdcB1,0,SW_AVERAGING);
			uDMAcontrolTable[UDMA_SEC_CHANNEL_ADC10].ulControl =
					savedUDMAcontrolWords[1][0];
#ifdef PROFILE
			c++;
#endif
		}
		if ((uDMAcontrolTable[UDMA_SEC_CHANNEL_ADC10 + UDMA_ALT_SELECT].ulControl
				& UDMA_CHCTL_XFERMODE_M) == UDMA_CHCTL_XFERMODE_STOP) {
//			adcB = rawAdcB2[0];
//			for (i = 1; i < SW_AVERAGING; i++)
//				adcB += rawAdcB2[i];
			adcB = arraysum(rawAdcB2,0,SW_AVERAGING);
			uDMAcontrolTable[UDMA_SEC_CHANNEL_ADC10 + UDMA_ALT_SELECT].ulControl =
					savedUDMAcontrolWords[1][1];
#ifdef PROFILE
			c++;
#endif
		}
//		HWREG(UDMA_ENASET) = 1 << UDMA_SEC_CHANNEL_ADC10; //MAP_uDMAChannelEnable(UDMA_SEC_CHANNEL_ADC10);
	}
//	if (adc0Status & ADC_INT_SS0)
//		MAP_ADCIntClear(ADC0_BASE, ADC_INT_SS0);
//	if (adc1Status & ADC_INT_SS0)
//		MAP_ADCIntClear(ADC1_BASE, ADC_INT_SS0);
#ifdef EXT_PROFILE
	GPIO_DISABLE_PIN(EXT_PROFILE_ADC_GPIO_BASE, EXT_PROFILE_ADC_GPIO_PIN);
#endif
#ifdef PROFILE
	adcIsrCounts[c]++;
	adcIsrClocks[c] = adcIsrStartClock - HWREG(NVIC_ST_CURRENT);
	adcIsrStartClocks[c] = adcIsrStartClock;
#endif
	__asm("cpsie i");
}

long reviveADC(void) {
	if (!MAP_uDMAChannelIsEnabled (UDMA_CHANNEL_ADC0)) {
		UARTprintf("\nADC0 uDMA channel disabled, enabling...\n");
		MAP_uDMAChannelEnable (UDMA_CHANNEL_ADC0);
	}
	if (!MAP_uDMAChannelIsEnabled (UDMA_SEC_CHANNEL_ADC10)) {
		UARTprintf("\nADC1 uDMA channel disabled, enabling...\n");
		MAP_uDMAChannelEnable (UDMA_SEC_CHANNEL_ADC10);
	}
	return 0;
}

#else

static tDMAControlTable uDMAcontrolTable[32] __attribute__((aligned(1024)));
static unsigned long savedUDMAcontrolWorld;

#define DECL_ADC_TO_FLOAT(suffix) \
	inline float adcToFloatA(long adcSample) { \
		return (((float) adcSample) - adcCenterVal##suffix) \
			* (1. /  2048.); \
	}
IMPL_ADC_TO_FLOAT(A)
IMPL_ADC_TO_FLOAT(B)

IMPL_SYNC_SYS_TICK(PWM_TIMER_BASE+TIMER_O_TAR)

long syncIO() {
	HWREG(TIMER0_BASE + TIMER_O_SYNC) = PWM_TIMER_A_SYNC | PWM_TIMER_B_SYNC;
	HWREG(TIMER0_BASE + TIMER_O_SYNC) = 0; // errata 9.1
	return 0;
}

void initADC(void) {
	MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
	MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC1);
	MAP_SysCtlPeripheralEnable(ADC0_PIN_PERIPH);
	MAP_SysCtlPeripheralEnable(ADC1_PIN_PERIPH);
	MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH2_UDMA);
	MAP_uDMAEnable();
	MAP_uDMAControlBaseSet(uDMAcontrolTable);
	MAP_uDMAChannelAssign(UDMA_CH17_ADC0_3);
	MAP_uDMAChannelAssign(UDMA_CH27_ADC1_3);
	MAP_uDMAChannelAttributeEnable(UDMA_CHANNEL_ADC3, UDMA_ATTR_HIGH_PRIORITY);
	MAP_uDMAChannelAttributeDisable(UDMA_CHANNEL_ADC3,
			UDMA_ATTR_ALTSELECT | UDMA_ATTR_USEBURST | UDMA_ATTR_REQMASK);
	MAP_uDMAChannelAttributeEnable(UDMA_SEC_CHANNEL_ADC13, UDMA_ATTR_HIGH_PRIORITY);
	MAP_uDMAChannelAttributeDisable(UDMA_SEC_CHANNEL_ADC13,
			UDMA_ATTR_ALTSELECT | UDMA_ATTR_USEBURST | UDMA_ATTR_REQMASK);
	if (ADC0_PIN_CFG)
	MAP_GPIOPinConfigure(ADC0_PIN_CFG);
	if (ADC1_PIN_CFG)
	MAP_GPIOPinConfigure(ADC1_PIN_CFG);
	MAP_GPIOPinTypeADC(ADC0_PIN_BASE, ADC0_PIN);
	MAP_GPIOPinTypeADC(ADC1_PIN_BASE, ADC1_PIN);
	resetADC();
	adcCenterValA = ADC_CENTER_VAL;
	adcCenterValB = ADC_CENTER_VAL;
}

long resetADC(void) {
	int i;
	MAP_SysCtlADCSpeedSet(SYSCTL_ADCSPEED_1MSPS);
	MAP_ADCHardwareOversampleConfigure(ADC0_BASE, HW_AVERAGING);
	MAP_ADCHardwareOversampleConfigure(ADC1_BASE, HW_AVERAGING);
	MAP_ADCResolutionSet(ADC0_BASE, ADC_RES_12BIT);
	MAP_ADCResolutionSet(ADC1_BASE, ADC_RES_12BIT);
	for (i = 0; i < 4; i++) {
		MAP_ADCSequenceDisable(ADC0_BASE, i);
		MAP_ADCSequenceDisable(ADC1_BASE, i);
	}
	MAP_ADCSequenceConfigure(ADC0_BASE, 3, ADC_TRIGGER_ALWAYS, 0);
	MAP_ADCSequenceStepConfigure(ADC0_BASE, 3, 0,
			ADC0_CH | ADC_CTL_IE | ADC_CTL_END);
	MAP_ADCSequenceConfigure(ADC1_BASE, 3, ADC_TRIGGER_ALWAYS, 0);
	MAP_ADCSequenceStepConfigure(ADC1_BASE, 3, 0,
			ADC1_CH | ADC_CTL_IE | ADC_CTL_END);

	MAP_uDMAChannelDisable(UDMA_CHANNEL_ADC0);
	MAP_uDMAChannelDisable(UDMA_SEC_CHANNEL_ADC10);
	MAP_uDMAChannelControlSet(UDMA_CHANNEL_ADC3,
			UDMA_SIZE_32 | UDMA_ARB_1 | UDMA_SRC_INC_NONE | UDMA_DST_INC_NONE);
	MAP_uDMAChannelControlSet(UDMA_SEC_CHANNEL_ADC13,
			UDMA_SIZE_32 | UDMA_ARB_1 | UDMA_SRC_INC_NONE | UDMA_DST_INC_NONE);
	MAP_uDMAChannelTransferSet(UDMA_CHANNEL_ADC3, UDMA_MODE_BASIC,
			ADC0_BASE + ADC_O_SSFIFO3, &adcA, 1024);
	MAP_uDMAChannelTransferSet(UDMA_SEC_CHANNEL_ADC13, UDMA_MODE_BASIC,
			ADC1_BASE + ADC_O_SSFIFO3, &adcB, 1024);
	savedUDMAcontrolWorld = uDMAcontrolTable[UDMA_CHANNEL_ADC3].ulControl;
	MAP_uDMAIntRegister(UDMA_INT_ERR, udmaIsr);
	MAP_uDMAChannelEnable(UDMA_CHANNEL_ADC3);
	MAP_uDMAChannelEnable(UDMA_SEC_CHANNEL_ADC13);
	MAP_ADCIntRegister(ADC0_BASE, 3, adcIsr);
	MAP_ADCIntRegister(ADC1_BASE, 3, adcIsr);
//	MAP_ADCIntEnable(ADC0_BASE, 3);
//	MAP_ADCIntEnable(ADC1_BASE, 3);
	MAP_ADCSequenceEnable(ADC0_BASE, 3);
	MAP_ADCSequenceEnable(ADC1_BASE, 3);
	return 0;
}

void adcIsr(void) {
	__asm("cpsid i");
	//	unsigned long adc0Status = MAP_ADCIntStatus(ADC0_BASE, 3, true), adc1Status =
	//			MAP_ADCIntStatus(ADC1_BASE, 3, true);
#ifdef PROFILE
	adcIsrStartClock = HWREG(NVIC_ST_CURRENT);
	adcIsrCounts++;
#endif
	unsigned long uDMAstatus = uDMAIntStatus();
	if (uDMAstatus & (1 << UDMA_CHANNEL_ADC3)) {
		MAP_uDMAIntClear(1 << UDMA_CHANNEL_ADC3);
//		MAP_ADCIntClear(ADC0_BASE, ADC_INT_SS3);
		uDMAcontrolTable[UDMA_CHANNEL_ADC3].ulControl = savedUDMAcontrolWorld;
		MAP_uDMAChannelEnable(UDMA_CHANNEL_ADC3);
	}
	if (uDMAstatus & (1 << UDMA_SEC_CHANNEL_ADC13)) {
		MAP_uDMAIntClear(1 << UDMA_SEC_CHANNEL_ADC13);
//		MAP_ADCIntClear(ADC1_BASE, ADC_INT_SS3);
		uDMAcontrolTable[UDMA_SEC_CHANNEL_ADC13].ulControl =
		savedUDMAcontrolWorld;
		MAP_uDMAChannelEnable(UDMA_SEC_CHANNEL_ADC13);
	}
//	if (adc0Status & ADC_INT_SS3)
//		MAP_ADCIntClear(ADC0_BASE, ADC_INT_SS3);
//	if (adc1Status & ADC_INT_SS3)
//		MAP_ADCIntClear(ADC1_BASE, ADC_INT_SS3);
#ifdef PROFILE
	adcIsrClocks = adcIsrStartClock - HWREG(NVIC_ST_CURRENT);
#endif
	__asm("cpsie i");
}

long reviveADC(void) {
	if(!MAP_uDMAChannelIsEnabled(UDMA_CHANNEL_ADC3)) {
		UARTprintf("\nADC0 uDMA channel disabled, enabling...\n");
		MAP_uDMAChannelEnable(UDMA_CHANNEL_ADC3);
	}
	if(!MAP_uDMAChannelIsEnabled(UDMA_SEC_CHANNEL_ADC13)) {
		UARTprintf("\nADC1 uDMA channel disabled, enabling...\n");
		MAP_uDMAChannelEnable(UDMA_SEC_CHANNEL_ADC13);
	}
	return 0;
}

#endif

inline void getAdcValues(float *xA, float *xB) {
	*xA = adcToFloatA(adcA);
	*xB = adcToFloatB(adcB);
}

void udmaIsr(void) {
	__asm("cpsid i");
	if (MAP_uDMAErrorStatusGet ()) {
		MAP_uDMAErrorStatusClear ();
		if (lastError == ERROR_NONE) {
			lastError = ERROR_DMA;
			UARTprintf("\nuDMA bus error\n");
		}
	}
	__asm("cpsie i");
}

void initPWM(void) {
	MAP_SysCtlPeripheralEnable (PWM_TIMER_PERIPH);
	MAP_SysCtlPeripheralEnable (PWM_TIMER_A_PIN_PERIPH);
	MAP_SysCtlPeripheralEnable (PWM_TIMER_B_PIN_PERIPH);
	MAP_GPIOPinConfigure (PWM_TIMER_A_PIN_CFG);
	MAP_GPIOPinTypeTimer (PWM_TIMER_A_PIN_BASE, PWM_TIMER_A_PIN);
	MAP_GPIOPadConfigSet (PWM_TIMER_A_PIN_BASE, PWM_TIMER_A_PIN,
			GPIO_STRENGTH_8MA_SC, GPIO_PIN_TYPE_STD);
	MAP_GPIOPinConfigure (PWM_TIMER_B_PIN_CFG);
	MAP_GPIOPinTypeTimer (PWM_TIMER_B_PIN_BASE, PWM_TIMER_B_PIN);
	MAP_GPIOPadConfigSet (PWM_TIMER_B_PIN_BASE, PWM_TIMER_B_PIN,
			GPIO_STRENGTH_8MA_SC, GPIO_PIN_TYPE_STD);
	resetPWM();
	pwmCenterDutyA = PWM_CENTER_DUTY;
	pwmCenterDutyB = PWM_CENTER_DUTY;
	dutyA = dutyB = PWM_CENTER_DUTY;
}

#define IMPL_FLOAT_TO_DUTY(suffix) \
	inline long floatToDuty##suffix(float x) { \
		long y = (long) (pwmCenterDuty##suffix - x * halfSamplePeriod); \
		if (y < 0) \
			return -1; \
		else if (y > samplePeriod) \
			return samplePeriod - 1; \
		else \
			return y - 1; \
	}
IMPL_FLOAT_TO_DUTY(A)
IMPL_FLOAT_TO_DUTY(B)

long resetPWM(void) {
	MAP_TimerDisable (PWM_TIMER_BASE, TIMER_BOTH);
	MAP_TimerConfigure (PWM_TIMER_BASE,
			TIMER_CFG_SPLIT_PAIR | TIMER_CFG_A_PWM | TIMER_CFG_B_PWM);
	MAP_TimerControlStall (PWM_TIMER_BASE, TIMER_BOTH, true);
	HWREG(PWM_TIMER_BASE + TIMER_O_TAMR) |= (TIMER_TAMR_TAMRSU
			| TIMER_TAMR_TAPLO | TIMER_TAMR_TAILD);
	HWREG(PWM_TIMER_BASE + TIMER_O_TBMR) |= (TIMER_TBMR_TBMRSU
			| TIMER_TBMR_TBPLO | TIMER_TBMR_TBILD);
	MAP_TimerLoadSet (PWM_TIMER_BASE, TIMER_BOTH, samplePeriod - 1);
	MAP_TimerMatchSet (PWM_TIMER_BASE, TIMER_BOTH, floatToDutyA(0.));
	dutyA = MAP_TimerMatchGet (PWM_TIMER_BASE, TIMER_A);
	dutyB = MAP_TimerMatchGet (PWM_TIMER_BASE, TIMER_B);
	MAP_TimerEnable (PWM_TIMER_BASE, TIMER_BOTH);
	return 0;
}

inline void setPWMDuties(float xA, float xB) {
	HWREG(PWM_TIMER_BASE + TIMER_O_TAMATCHR) = floatToDutyA(xA);
	HWREG(PWM_TIMER_BASE + TIMER_O_TBMATCHR) = floatToDutyB(xB);
}
long frequencyToClockCycles(float f) {
	return (long) (MAP_SysCtlClockGet () / f);
}
float clockCyclesToFrequency(long cycles) {
	return MAP_SysCtlClockGet () / ((float) cycles);
}
void updateAUXTimerValue(void *var, void *newVal) {
	long s,t;
	if (var == &auxFreqA) {
		t = frequencyToClockCycles(*(float*) newVal);
		MAP_TimerLoadSet (AUX_TIMER_BASE, AUX_TIMER_A, t - 1);
		auxFreqA = clockCyclesToFrequency(t);
		s = (long) (t * (1. - auxDutyA));
		MAP_TimerMatchSet (AUX_TIMER_BASE, AUX_TIMER_A,	s - 1);
		auxDutyA = 1. - s / (float) t;
	} else if (var == &auxFreqB) {
		t = frequencyToClockCycles(*(float*) newVal);
		MAP_TimerLoadSet (AUX_TIMER_BASE, AUX_TIMER_B, t - 1);
		auxFreqB = clockCyclesToFrequency(t);
		s = (long) (t * (1. - auxDutyB));
		MAP_TimerMatchSet (AUX_TIMER_BASE, AUX_TIMER_B, s - 1);
		auxDutyB = 1. - s / (float) t;
	} else if (var == &auxDutyA) {
		t = frequencyToClockCycles(auxFreqA);
		s = (long) (t * (1. - *(float*) newVal));
		MAP_TimerMatchSet (AUX_TIMER_BASE, AUX_TIMER_A, s -1);
		auxDutyA = 1. - s / (float) t;
	} else if (var == &auxDutyB) {
		t = frequencyToClockCycles(auxFreqB);
		s = (long) (t * (1. - *(float*) newVal));
		MAP_TimerMatchSet (AUX_TIMER_BASE, AUX_TIMER_B,	s - 1);
		auxDutyB = 1. - s / (float) t;
	}
}
void adjustAUXTimer(void) {
	updateAUXTimerValue(&auxFreqA, &auxFreqA);
	updateAUXTimerValue(&auxDutyA, &auxDutyA);
	updateAUXTimerValue(&auxFreqB, &auxFreqB);
	updateAUXTimerValue(&auxDutyB, &auxDutyB);
}
void initAUXTimer(void) {
	MAP_SysCtlPeripheralEnable (AUX_TIMER_PERIPH);
	MAP_SysCtlPeripheralEnable (AUX_TIMER_A_PIN_PERIPH);
	MAP_SysCtlPeripheralEnable (AUX_TIMER_B_PIN_PERIPH);
	MAP_GPIOPinConfigure (AUX_TIMER_A_PIN_CFG);
	MAP_GPIOPinTypeTimer (AUX_TIMER_A_PIN_BASE, AUX_TIMER_A_PIN);
	MAP_GPIOPadConfigSet (AUX_TIMER_A_PIN_BASE, AUX_TIMER_A_PIN,
			GPIO_STRENGTH_8MA_SC, GPIO_PIN_TYPE_STD);
	MAP_GPIOPinConfigure (AUX_TIMER_B_PIN_CFG);
	MAP_GPIOPinTypeTimer (AUX_TIMER_B_PIN_BASE, AUX_TIMER_B_PIN);
	MAP_GPIOPadConfigSet (AUX_TIMER_B_PIN_BASE, AUX_TIMER_B_PIN,
			GPIO_STRENGTH_8MA_SC, GPIO_PIN_TYPE_STD);
	resetAUXTimer();
}
void resetAUXTimer(void) {
	MAP_TimerDisable (AUX_TIMER_BASE, TIMER_BOTH);
	MAP_TimerConfigure (AUX_TIMER_BASE,
			TIMER_CFG_SPLIT_PAIR | TIMER_CFG_A_PWM | TIMER_CFG_B_PWM);
	MAP_TimerControlStall (AUX_TIMER_BASE, TIMER_BOTH, false);
	HWREG(AUX_TIMER_BASE + TIMER_O_TAMR) |= (TIMER_TAMR_TAMRSU
			| TIMER_TAMR_TAPLO | TIMER_TAMR_TAILD);
	HWREG(AUX_TIMER_BASE + TIMER_O_TBMR) |= (TIMER_TBMR_TBMRSU
			| TIMER_TBMR_TBPLO | TIMER_TBMR_TBILD);
	adjustAUXTimer();
	MAP_TimerEnable (AUX_TIMER_BASE, TIMER_BOTH);
}

#define IMPL_TIMER_OFFSET(procName,timerA,timerB) \
	long procName##InClocks() { \
	long a1, b1, b2, a2,b3,a3,a4,b4; \
		__asm("cpsid i"); \
		a1 = HWREG(timerA); \
		b1 = HWREG(timerB); \
		b2 = HWREG(timerB); \
		a2 = HWREG(timerA); \
		b3 = HWREG(timerB); \
		a3 = HWREG(timerA); \
		a4 = HWREG(timerA); \
		b4 = HWREG(timerB); \
		__asm("cpsie i"); \
		if (a1 <= a2) \
			a1 += samplePeriod; \
		if (b1 <= b2) \
			b1 += samplePeriod; \
		if (a3 <= a4) \
			a3 += samplePeriod; \
		if (b3 <= b4) \
			b3 += samplePeriod; \
		return ((a1 + a2 + a3 + a4) - (b1 + b2 + b3 + b4)) / 4; \
	} \
	long procName() { \
		char buf[16]; \
		floatToString(((float) procName##InClocks()) / MAP_SysCtlClockGet (), \
				buf, sizeof(buf)); \
		UARTprintf("%ss", buf); \
		return 0; \
	}
//long ioOffsetInClocks() {
//	long a1, b1, b2, a2,b3,a3,a4,b4;
//	__asm("cpsid i");
//	a1 = HWREG(ADC_TIMER_BASE+TIMER_O_TBR);
//	b1 = HWREG(PWM_TIMER_BASE+TIMER_O_TAR);
//	b2 = HWREG(PWM_TIMER_BASE+TIMER_O_TAR);
//	a2 = HWREG(ADC_TIMER_BASE+TIMER_O_TBR);
//	b3 = HWREG(PWM_TIMER_BASE+TIMER_O_TAR);
//	a3 = HWREG(ADC_TIMER_BASE+TIMER_O_TBR);
//	a4 = HWREG(ADC_TIMER_BASE+TIMER_O_TBR);
//	b4 = HWREG(PWM_TIMER_BASE+TIMER_O_TAR);
//	__asm("cpsie i");
//	if (a1 <= a2)
//		a1 += samplePeriod;
//	if (b1 <= b2)
//		b1 += samplePeriod;
//	if (a3 <= a4)
//		a3 += samplePeriod;
//	if (b3 <= b4)
//		b3 += samplePeriod;
//	return ((a1 + a2 + a3 + a4) - (b1 + b2 + b3 + b4)) / 4;
//}
//long ioOffset() {
//	char buf[16];
//	floatToString(((float) ioOffsetInClocks()) / MAP_SysCtlClockGet (), buf,
//			sizeof(buf));
//	UARTprintf("%ss", buf);
//	return 0;
//}
IMPL_TIMER_OFFSET(ioOffset, REF_TIMER_BASE+TIMER_O_TBR,
		PWM_TIMER_BASE+TIMER_O_TAR)
//long outputSkewInClocks() {
//	long a1, b1, b2, a2,b3,a3,a4,b4;
//	__asm("cpsid i");
//	a1 = HWREG(PWM_TIMER_BASE+TIMER_O_TAR);
//	b1 = HWREG(PWM_TIMER_BASE+TIMER_O_TBR);
//	b2 = HWREG(PWM_TIMER_BASE+TIMER_O_TBR);
//	a2 = HWREG(PWM_TIMER_BASE+TIMER_O_TAR);
//	b3 = HWREG(PWM_TIMER_BASE+TIMER_O_TBR);
//	a3 = HWREG(PWM_TIMER_BASE+TIMER_O_TAR);
//	a4 = HWREG(PWM_TIMER_BASE+TIMER_O_TAR);
//	b4 = HWREG(PWM_TIMER_BASE+TIMER_O_TBR);
//	__asm("cpsie i");
//	if (a1 <= a2)
//		a1 += samplePeriod;
//	if (b1 <= b2)
//		b1 += samplePeriod;
//	if (a3 <= a4)
//		a3 += samplePeriod;
//	if (b3 <= b4)
//		b3 += samplePeriod;
//	return ((a1 + a2 + a3 + a4) - (b1 + b2 + b3 + b4)) / 4;
//}
//long outputSkew() {
//	char buf[16];
//	floatToString(((float) outputSkewInClocks()) / MAP_SysCtlClockGet (), buf,
//			sizeof(buf));
//	UARTprintf("%ss", buf);
//	return 0;
//}
IMPL_TIMER_OFFSET(outputSkew, PWM_TIMER_BASE+TIMER_O_TAR,
		PWM_TIMER_BASE+TIMER_O_TBR)
IMPL_TIMER_OFFSET(sysTickOffset, REF_TIMER_BASE+TIMER_O_TBR, NVIC_ST_CURRENT)

#ifdef EXT_PROFILE
void initExternalProfiling() {
	MAP_SysCtlPeripheralEnable (EXT_PROFILE_MAIN_GPIO_PERIPH);
	MAP_GPIOPinTypeGPIOOutput (EXT_PROFILE_MAIN_GPIO_BASE,
			EXT_PROFILE_MAIN_GPIO_PIN);
	MAP_GPIOPadConfigSet (EXT_PROFILE_MAIN_GPIO_BASE, EXT_PROFILE_MAIN_GPIO_PIN,
			GPIO_STRENGTH_8MA_SC, GPIO_PIN_TYPE_STD);
	MAP_SysCtlPeripheralEnable (EXT_PROFILE_ADC_GPIO_PERIPH);
	MAP_GPIOPinTypeGPIOOutput (EXT_PROFILE_ADC_GPIO_BASE,
			EXT_PROFILE_ADC_GPIO_PIN);
	MAP_GPIOPadConfigSet (EXT_PROFILE_ADC_GPIO_BASE, EXT_PROFILE_ADC_GPIO_PIN,
			GPIO_STRENGTH_8MA_SC, GPIO_PIN_TYPE_STD);

}
#endif

#include <math.h>
long zeroAdjustInput(long *adcVal, float *adcCenterVal) {
	enum Mode m = mode;
	char buf[16];
	mode = MODE_USER;
	// gnd input ??
	volatile unsigned long lTick = tick;
	volatile long sum = 0;
	volatile unsigned long sumSq = 0, collectedSamples = 0;
	while (collectedSamples < ZERO_ADJUST_AVERAGES) {
		while (tick == lTick)
			__asm("");
		__asm("cpsid i");
		lTick = tick;
		sum += *adcVal - SW_AVERAGING * 2048;
		sumSq += (*adcVal - SW_AVERAGING * 2048)
				* (*adcVal - SW_AVERAGING * 2048);
		collectedSamples++;
		__asm("cpsie i");
	}
	float mean = (((float) sum) / collectedSamples);
	*adcCenterVal = mean + SW_AVERAGING * 2048;
	floatToString(*adcCenterVal, buf, sizeof(buf));
	UARTprintf("%s", buf);
	floatToString(sqrt((((float) sumSq) / collectedSamples) - mean * mean), buf,
			sizeof(buf));
	UARTprintf("±%s\n", buf);
	// un-gnd input ??
	mode = m;

	return 0;
}
long zeroAdjustInputA() {
	return zeroAdjustInput(&adcA, &adcCenterValA);
}
long zeroAdjustInputB() {
	return zeroAdjustInput(&adcB, &adcCenterValB);
}
long zeroAdjustInputs() {
	enum Mode m = mode;
	mode = MODE_USER;
	// gnd input ??
	volatile long lTick = tick;
	volatile long sumA = 0, sumB = 0, collectedSamples = 0;
	while (collectedSamples < ZERO_ADJUST_AVERAGES) {
		while (tick == lTick)
			__asm("");
		__asm("cpsid i");
		lTick = tick;
		sumA += adcA;
		sumB += adcB;
		collectedSamples++;
		__asm("cpsie i");
	}
	adcCenterValA = ((float) sumA) / collectedSamples;
	adcCenterValB = ((float) sumB) / collectedSamples;
	// un-gnd input ??
	mode = m;
	return 0;
}
