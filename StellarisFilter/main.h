/*
 * main.h
 *
 *  Created on: 04.04.2013
 *      Author: rj
 */

#ifndef MAIN_H_
#define MAIN_H_

#include "inc/hw_types.h"

#define PROFILE
#define EXT_PROFILE

#define NULL (void*)0

#define min(x,y) (x<y?x:y)
#define max(x,y) (x>y?x:y)
#define abs(x)   (x<0?-x:x)

enum Mode {
	MODE_FREEZE = 0,
	MODE_FUNC_GEN = 1,
	MODE_USER = 2,
	MODE_THROUGH = 3,
	MODE_FILTER = 4,
	MAX_MODE = 0xFFFFFFFF
};
enum Error {
	ERROR_NONE = 0,
	ERROR_MODE = 1,
	ERROR_DMA = 2,
	MAX_ERROR = 0xFFFFFFFF
};

long clock();
long syncSysTick();

#endif /* MAIN_H_ */
